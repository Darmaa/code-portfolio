#include "pid.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

#define DEFAULT_PID_INTEGRATION_LIMIT 5000.0

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void pid_init( pid *ppid, const float desired, const float kp,
			  const float ki, const float kd, const float dt){
	ppid->error 	= 0;
	ppid->prevError = 0;
	ppid->integ     = 0;
	ppid->deriv     = 0;
	ppid->desired   = desired;
	ppid->kp = kp;
	ppid->ki = ki;
	ppid->kd = kd;
	ppid->iLimit = DEFAULT_PID_INTEGRATION_LIMIT;
	ppid->iLimitLow = -DEFAULT_PID_INTEGRATION_LIMIT;
	ppid->dt = dt;

	ppid->outD = 0;
	ppid->outI = 0;
	ppid->outP = 0;
}

void pid_reset( pid *ppid ){
	ppid->error = 0;
	ppid->integ = 0;
	ppid->deriv = 0;
	ppid->prevError = 0;
}

bool pid_is_active( pid *ppid ){
	if (ppid->kp < 0.0001 && ppid->ki < 0.0001 && ppid->kd < 0.0001){
		return true;
	} else {
		return false;
	}
}

float pid_update( pid *ppid, const float measured, const bool update_error){
	float output;
	if (update_error){
		ppid->error = ppid->desired - measured;
	}
	ppid->integ += ppid->error * ppid->dt;
	if (ppid->integ > ppid->iLimit){
		ppid->integ = ppid->iLimit;
	} else if (ppid->integ < ppid->iLimitLow){
		ppid->integ = ppid->iLimitLow;
	}
	ppid->deriv =( ppid->error - ppid->prevError) / ppid->dt;
	ppid->outP  =  ppid->kp * ppid->error;
	ppid->outI  =  ppid->ki * ppid->integ;
	ppid->outD  =  ppid->kd * ppid->deriv;
	output = ppid->outP + ppid->outI + ppid->outD;
	ppid->prevError = ppid->error;
	return output;
}

float pid_get_desired( pid *ppid ){
	return ppid->desired;
}

void pid_set_integral_limit( pid *ppid, const float high, const float low ) {
	ppid->iLimit = high;
	ppid->iLimitLow = low;
}

void pid_set_desired( pid *ppid, const float desired){
	ppid->desired = desired;
}

void pid_set_error( pid *ppid, const float error){
	ppid->error = error;
}

void pid_set_kp( pid *ppid, const float kp){
	ppid->kp = kp;
}

void pid_set_ki( pid *ppid, const float ki){
	ppid->ki = ki;
}

void pid_set_kd( pid *ppid, const float kd){
	ppid->kd = kd;
}

void pid_set_dt( pid *ppid, const float dt) {
	ppid->dt = dt;
}

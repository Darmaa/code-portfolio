

#ifndef PID_H_
#define PID_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	float desired; 		// set point
	float error; 		// error
	float prevError; 	// previous error
	float integ; 		// integral
	float deriv; 		// derivative
	float kp; 			// proportional gain
	float ki; 			// integral gain
	float kd; 			// derivative gain
	float iLimit; 		// integral limit
	float iLimitLow; 	// integral limit
	float dt; 			// delta-time ( period )

	float outP; 		// proportional output 	( for debugging )
	float outI; 		// integral output 		( for debugging )
	float outD; 		// derivative output 	( for debugging )
} pid;

void pid_init( pid *ppid, const float desired, const float kp, const float ki, const float kd, const float dt );
void pid_reset(pid *ppid);
bool pid_is_active(pid *ppid);

float pid_update( pid *ppid, const float measured, const bool update_error );
float pid_get_desired( pid *ppid );

void pid_set_integral_limit( pid *ppid, const float high, const float low );
void pid_set_desired( pid *ppid, const float desired );
void pid_set_error( pid *ppid, const float error );
void pid_set_kp( pid *ppid, const float kp );
void pid_set_ki( pid *ppid, const float ki );
void pid_set_kd( pid *ppid, const float kd );
void pid_set_dt( pid *ppid, const float dt );

#endif /* PID_H_ */

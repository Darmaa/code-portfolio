#ifndef COROUTINE_H_
#define COROUTINE_H_

#include <stdint.h>

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

typedef enum coroutine_state {
	e_frozen  = 0x00000000,
	e_running = 0xffffffff
} coroutine_state_t;

typedef struct {
	uint32_t state;
} coroutine_control_t;

#define INTERVAL_SCALE_2 	0x00000001	  	// T = INTERVAL*2^1
#define INTERVAL_SCALE_4 	0x00000003	  	// T = INTERVAL*2^2
#define INTERVAL_SCALE_8 	0x00000007	  	// T = INTERVAL*2^3
#define INTERVAL_SCALE_16 	0x0000000f	  	// T = INTERVAL*2^4
#define INTERVAL_SCALE_32 	0x0000001f	  	// T = INTERVAL*2^5
#define INTERVAL_SCALE_64 	0x0000003f	  	// T = INTERVAL*2^6
#define INTERVAL_SCALE_128 	0x0000007f	  	// T = INTERVAL*2^7
#define INTERVAL_SCALE_256 	0x000000ff	  	// T = INTERVAL*2^8
#define INTERVAL_SCALE_512 	0x000001ff	  	// T = INTERVAL*2^9
#define INTERVAL_SCALE_1024	0x000003ff	  	// T = INTERVAL*2^10

#define COROUTINE_TIME_SCALER(coroutine,tick,SCALER) ((coroutine.state & tick & SCALER ) == SCALER )
#define COROUTINE_ASSERT(coroutine)  ( coroutine.state == e_running )
#define COROUTINE_SUSPEND(coroutine) do{ coroutine.state = e_frozen;  } while (0)
#define COROUTINE_RESUME(coroutine)	 do{ coroutine.state = e_running; } while (0)

#endif /* COROUTINE_H_ */

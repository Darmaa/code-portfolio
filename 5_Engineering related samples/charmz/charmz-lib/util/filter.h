
#ifndef FILTER_H_
#define FILTER_H_

#include <stdint.h>

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

// First order IIR filter
typedef struct {
	float x_1;
	float y_1;

	float a_2;

	float b_1;
	float b_2;
} iir_filter;

// First order RC filter
typedef struct {
	float y;
	float alpha;
} rc_filter;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

void iir_init( iir_filter *filt, const float *a, const float *b , const float x_0 );
float iir_filtering( float x_0, iir_filter *filter );

void rc_init( rc_filter *filt, const float alpha );
float rc_filtering( float x, rc_filter *filter );

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* FILTER_H_ */

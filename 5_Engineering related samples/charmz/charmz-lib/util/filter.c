
#include <math.h>
#include "filter.h"

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void iir_init( iir_filter *filter, const float *a, const float *b, const float x_0 ){
	filter->b_1 = b[0]/a[0];
	filter->b_2 = b[1]/a[0];

	filter->a_2 = a[1]/a[0];
	filter->x_1 = x_0;
	filter->y_1 = x_0;
}

float iir_filtering( const float x_0, iir_filter *filter ){
	float y_1 = x_0 * filter->b_1 + filter->x_1 * filter->b_2 - filter->y_1 * filter->a_2;
	filter->x_1 = x_0;
	filter->y_1 = y_1;
	return y_1;
}

void rc_init( rc_filter *filter, const float alpha ){
	filter->y = 0.0;
	filter->alpha = alpha;
}

float rc_filtering( const float x, rc_filter *filter ){
	float y_1 = filter->y * filter->alpha + x * (1 - filter->alpha);
	filter->y = y_1;
	return y_1;
}

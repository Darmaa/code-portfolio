
#ifndef MAVLINK_INTERFACE_H_
#define MAVLINK_INTERFACE_H_

#include "../queue.h"
#include "mavlink/common/mavlink.h"

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

#define MAV_SYSTEM_ID 				(100) 			//
#define MAV_AUTOPILOT_GENERIC 		(0) 			// Generic autopilot, full support for everything
//#define MAV_MODE_MANUAL_DISARMED 	(64) 			// System is allowed to be active, under manual (RC)
#define MAV_CONNECTION_ALIVE		(2000/50)		// connection timeout 2000ms | check period 50ms
#define MAV_CONNECTION_LOST			(-1)
#define MAV_ONBOARD_PARAM_COUNT 	30				//

typedef struct{
	uint16_t pointer;
	float *param[MAV_ONBOARD_PARAM_COUNT];
	char param_name[MAV_ONBOARD_PARAM_COUNT][MAVLINK_MSG_PARAM_SET_FIELD_PARAM_ID_LEN];
} mav_global_struct;

typedef struct{
	mav_global_struct global_data;

	queue_t receive_message_queue;
	queue_t transmit_message_queue;

	uint8_t system_type;
	uint8_t autopilot_type;

	uint8_t  system_mode;
	uint32_t custom_mode;
	uint8_t  system_state;
	uint16_t parameter_i;
	int16_t  connection_timeout;

	mavlink_system_t 		mavlink_system;
	mavlink_message_t 		msg;
	mavlink_param_set_t 	set_param;
	mavlink_sys_status_t 	system_status;

	bool alive;
	bool enable;
} mavcomm_t;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

void mavlink_init( mavcomm_t *mavlink );

void mavlink_transmit_queue_push( mavcomm_t *mavlink, mavlink_message_t *msg );
uint16_t mavlink_transmit_queue_pull( mavcomm_t *mavlink, uint8_t *data );

void mavlink_receive_queue_push( mavcomm_t *mavlink, uint8_t *data, uint16_t size );
uint16_t mavlink_receive_queue_pull( mavcomm_t *mavlink, uint8_t *data );

void mavlink_parameter_reset( mavcomm_t *mavlink );
void mavlink_parameter_register( mavcomm_t *mavlink, float *param, const char *name );
int mavlink_parameter_update( mavcomm_t *mavlink, char *name, float value );
void mavlink_parameter_publish( mavcomm_t *mavlink, uint16_t index );

bool mavlink_message_parse( mavcomm_t *mavlink, mavlink_message_t *msg );

//bool mavlink_message_parse( mavcomm_t *mavlink );
//void mavlink_message_parse(void);
//void mavlink_message_put( uint8_t *msg, uint16_t *size );
//void mavlink_message_get( uint8_t *msg, uint16_t *size );
//bool mavlink_message_available(void);
//void mavlink_message_actuator( uint16_t m1, uint16_t m2, uint16_t m3, uint16_t m4, uint32_t timestamp  );
//void mavlink_message_attitude( float roll, float pitch, float yaw, float gx, float gy, float gz, uint32_t timestamp );
//void mavlink_message_position( float x, float y, float z );
//bool mavlink_connection_alive(void);
//void mavlink_set_system_status( uint8_t s_state, uint32_t c_state, float vbat );

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* MAVLINK_INTERFACE_H_ */

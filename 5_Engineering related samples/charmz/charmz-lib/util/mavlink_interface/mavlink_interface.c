
#include "mavlink_interface.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

static mavlink_message_t mav_msg;

uint8_t message_buffer[MAVLINK_MAX_PACKET_LEN];

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

void mavlink_init( mavcomm_t *mavlink )
{
	mavlink->system_type 			= MAV_TYPE_QUADROTOR;		// Define the system type, in this case an airplane
	mavlink->autopilot_type 		= 10; 						// MAV_AUTOPILOT_SPARK_QCOPTER;
	mavlink->system_mode  			= MAV_MODE_MANUAL_DISARMED;
	mavlink->custom_mode  			= 0;
	mavlink->system_state 			= MAV_STATE_UNINIT;
	mavlink->parameter_i  			= 0;
	mavlink->connection_timeout 	= MAV_CONNECTION_LOST;
	mavlink->mavlink_system.sysid 	= MAV_SYSTEM_ID;

	mavlink->system_status.onboard_control_sensors_present = MAV_SYS_STATUS_SENSOR_3D_GYRO  |
												    		 MAV_SYS_STATUS_SENSOR_3D_ACCEL |
															 MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE;

	mavlink->system_status.onboard_control_sensors_enabled = MAV_SYS_STATUS_SENSOR_3D_GYRO  |
		    												 MAV_SYS_STATUS_SENSOR_3D_ACCEL |
															 MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE;

	mavlink->system_status.onboard_control_sensors_health = MAV_SYS_STATUS_SENSOR_3D_GYRO   |
															MAV_SYS_STATUS_SENSOR_3D_ACCEL  |
															MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE;
	queue_init( &mavlink->receive_message_queue );
	queue_init( &mavlink->transmit_message_queue );

	mavlink->enable = false;
	mavlink->alive = false;
	mavlink_parameter_reset( mavlink );
}

void mavlink_parameter_reset( mavcomm_t *mavlink )
{
	mavlink->global_data.pointer = 0;
	uint16_t i = 0;
	for(; i < MAV_ONBOARD_PARAM_COUNT; i++ ){
		mavlink->global_data.param[i] = NULL;
		strcpy(mavlink->global_data.param_name[i], "NON");
	}
}

void mavlink_parameter_register( mavcomm_t *mavlink, float *param, const char *name )
{
	if( mavlink->global_data.pointer < MAV_ONBOARD_PARAM_COUNT ){
		mavlink->global_data.param[mavlink->global_data.pointer] = param;
		memset(mavlink->global_data.param_name[mavlink->global_data.pointer],0,MAVLINK_MSG_PARAM_SET_FIELD_PARAM_ID_LEN*sizeof(char));
		strcpy(mavlink->global_data.param_name[mavlink->global_data.pointer], name);
		mavlink->global_data.pointer++;
	}
}

int mavlink_parameter_update( mavcomm_t *mavlink, char *name, float value )
{
	uint16_t i = 0;
	for( ; i < mavlink->global_data.pointer; i++ ){
		if( strcmp( name, mavlink->global_data.param_name[i] ) == 0 ){
			if (*mavlink->global_data.param[i] != value && !isnan(value) && !isinf(value)){
				*mavlink->global_data.param[i] = value;
				mavlink_parameter_publish( mavlink, i );
			}
			break;
		}
	}
	return (int)i;
}

void mavlink_parameter_publish( mavcomm_t *mavlink, uint16_t index )
{
	// Pack the message
	mavlink_msg_param_value_pack( mavlink->mavlink_system.sysid, MAV_COMP_ID_SYSTEM_CONTROL, &mav_msg,
			mavlink->global_data.param_name[index],*mavlink->global_data.param[index],
			MAVLINK_TYPE_FLOAT, mavlink->global_data.pointer, index );
	mavlink_transmit_queue_push( mavlink, &mav_msg );
}

/*
void mavlink_parameter_reset( mavcomm_t *mavlink )
{
	mavlink->global_data.pointer = 0;
	uint8_t i = 0;
	for(; i < MAV_ONBOARD_PARAM_COUNT; i++ ){
		mavlink->global_data.param[i] = NULL;
		strcpy(mavlink->global_data.param_name[i], "NON");
	}

	strcpy(mavlink->global_data.param_name[1], "ROLL I");
	strcpy(mavlink->global_data.param_name[2], "ROLL D");
	strcpy(mavlink->global_data.param_name[3], "ROLL IHigh");
	strcpy(mavlink->global_data.param_name[4], "ROLL ILow");

	strcpy(mavlink->global_data.param_name[5], "ROLL RATE P");
	strcpy(mavlink->global_data.param_name[6], "ROLL RATE I");
	strcpy(mavlink->global_data.param_name[7], "ROLL RATE D");
	strcpy(mavlink->global_data.param_name[8], "ROLL RATE IHigh");
	strcpy(mavlink->global_data.param_name[9], "ROLL RATE ILow");

	strcpy(mavlink->global_data.param_name[10], "PITCH P");
	strcpy(mavlink->global_data.param_name[11], "PITCH I");
	strcpy(mavlink->global_data.param_name[12], "PITCH D");
	strcpy(mavlink->global_data.param_name[13], "PITCH IHigh");
	strcpy(mavlink->global_data.param_name[14], "PITCH ILow");

	strcpy(mavlink->global_data.param_name[15], "PITCH RATE P");
	strcpy(mavlink->global_data.param_name[16], "PITCH RATE I");
	strcpy(mavlink->global_data.param_name[17], "PITCH RATE D");
	strcpy(mavlink->global_data.param_name[18], "PITCH RATE IHigh");
	strcpy(mavlink->global_data.param_name[19], "PITCH RATE ILow");

	strcpy(mavlink->global_data.param_name[20], "YAW P");
	strcpy(mavlink->global_data.param_name[21], "YAW I");
	strcpy(mavlink->global_data.param_name[22], "YAW D");
	strcpy(mavlink->global_data.param_name[23], "YAW IHigh");
	strcpy(mavlink->global_data.param_name[24], "YAW ILow");

	strcpy(mavlink->global_data.param_name[25], "YAW RATE P");
	strcpy(mavlink->global_data.param_name[26], "YAW RATE I");
	strcpy(mavlink->global_data.param_name[27], "YAW RATE D");
	strcpy(mavlink->global_data.param_name[28], "YAW RATE IHigh");
	strcpy(mavlink->global_data.param_name[29], "YAW RATE ILow");
*/

void mavlink_set_param( mavlink_message_t *msg )
{
//	mavlink_msg_param_set_decode(msg, &m_set_param);
//	char* param_name = m_set_param.param_id;
//	uint16_t i;
//	for( i = 0; i < MAV_ONBOARD_PARAM_COUNT; i++ ){
//		if( strcmp( param_name, global_data.param_name[i] ) == 0 ){
//			if (global_data.param[i] != m_set_param.param_value
//				&& !isnan(m_set_param.param_value) && !isinf(m_set_param.param_value))
//			{
//				global_data.param[i] = m_set_param.param_value;
//				parameter_handler( global_data.param );
//
//				mavlink_msg_param_value_pack(mavlink_system.sysid, MAV_COMP_ID_SYSTEM_CONTROL, msg,
//						param_name, global_data.param[i], MAVLINK_TYPE_FLOAT, ONBOARD_PARAM_COUNT, i );
//
//				mavlink_msg_to_queue( msg );
//			}
//			break;
//		}
//	}
}



void mavlink_transmit_queue_push( mavcomm_t *mavlink, mavlink_message_t *msg )
{
	uint16_t size = mavlink_msg_to_send_buffer( message_buffer, msg );
	for( uint16_t i = 0; i < size; i++ ){
		queue_push( &mavlink->transmit_message_queue, &message_buffer[i] );
	}
}

uint16_t mavlink_transmit_queue_pull( mavcomm_t *mavlink, uint8_t *data )
{
	uint16_t size = queue_size( &mavlink->transmit_message_queue );
	for( uint16_t i = 0; i < size; i++ ){
		queue_pull( &mavlink->transmit_message_queue, &data[i] );
	}
	return size;
}

void mavlink_receive_queue_push( mavcomm_t *mavlink, uint8_t *data, uint16_t size )
{
	for( uint16_t i = 0; i < size; i++ ){
		queue_push( &mavlink->receive_message_queue, &data[i] );
	}
}

uint16_t mavlink_receive_queue_pull( mavcomm_t *mavlink, uint8_t *data )
{
	return queue_pull( &mavlink->receive_message_queue, data );
}


bool mavlink_message_parse( mavcomm_t *mavlink, mavlink_message_t *msg )
{
	mavlink_status_t status;
	uint8_t c;
	while( queue_pull( &mavlink->receive_message_queue, &c ) ){
		// Try to get a new message
		if( mavlink_parse_char( MAVLINK_COMM_0, c, msg, &status ) ) {
			mavlink->connection_timeout = MAV_CONNECTION_ALIVE;
			return true;
		}
	}
	// Check connection
	if( mavlink->connection_timeout > 0 ) {
		mavlink->connection_timeout--;
	} else if( mavlink->connection_timeout == 0 ) {
		mavlink->system_mode  = MAV_MODE_MANUAL_DISARMED;
		mavlink->connection_timeout = MAV_CONNECTION_LOST;
	}
	return false;
}

/*
void mavlink_message_parse( mavcomm_t *mavlink )
{
	mavlink_status_t status;
	mavlink_message_t msg;

	uint16_t size = mavlink_queue_size( &mavlink->receive_message_queue );
	if( size > 0 ){
		for( uint16_t i = 0; i < size; i++ ){
			uint8_t c;
			mavlink_queue_pull( &mavlink->receive_message_queue, &c );
			// Try to get a new message
			if( mavlink_parse_char( MAVLINK_COMM_0, c, &msg, &status ) ) {
				// Handle message
				mavlink_message_handle( &msg );

				mavlink->connection_timeout = MAV_CONNECTION_ALIVE;
			}
		}
	}
	// Send parameters one by one
	if (mavlink->parameter_i < MAV_ONBOARD_PARAM_COUNT){
		// Pack the message
		mavlink_msg_param_value_pack( mavlink->mavlink_system.sysid, MAV_COMP_ID_SYSTEM_CONTROL, &msg,
									  global_data.param_name[mavlink->parameter_i],global_data.param[mavlink->parameter_i],
									  MAVLINK_TYPE_FLOAT, MAV_ONBOARD_PARAM_COUNT, mavlink->parameter_i );

		mavlink_msg_to_queue( &msg );
		mavlink->parameter_i++;
	}

	if( mavlink->connection_timeout > 0 ) {
		mavlink->connection_timeout--;
	} else if( mavlink->connection_timeout == 0 ) {
		mavlink->system_mode  = MAV_MODE_MANUAL_DISARMED;
		mavlink->connection_timeout = MAV_CONNECTION_LOST;
	}
}*/

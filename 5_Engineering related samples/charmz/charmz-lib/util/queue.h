
#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdint.h>
#include <stdbool.h>

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

#define DEFAULT_QUEUE_LENGTH 512

typedef struct{
	uint32_t mask;
	uint8_t  buffer[DEFAULT_QUEUE_LENGTH];
	uint32_t read_pos;
	uint32_t write_pos;
} queue_t;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

void queue_init( queue_t *queue );
uint32_t queue_size( queue_t *queue );
bool queue_pull( queue_t *queue, uint8_t *data );
bool queue_push( queue_t *queue, uint8_t *data );
void queue_flush( queue_t *queue );

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* QUEUE_H_ */

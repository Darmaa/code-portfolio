
#include "queue.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void queue_init( queue_t *queue )
{
	queue->mask = DEFAULT_QUEUE_LENGTH - 1;
	queue->read_pos = 0;
	queue->write_pos = 0;
}

uint32_t queue_size( queue_t *queue )
{
	return ( queue->write_pos - queue->read_pos );
}

bool queue_pull( queue_t *queue, uint8_t *data )
{
	if( queue_size(queue) != 0 ){
		*data = queue->buffer[ queue->read_pos & queue->mask ];
		queue->read_pos++;
		return true;
	} else {
		return false;
	}
}

bool queue_push( queue_t *queue, uint8_t *data )
{
	if( queue_size(queue) < queue->mask ){
		queue->buffer[ queue->write_pos & queue->mask ] = *data;
		queue->write_pos++;
		return true;
	} else {
		return false;
	}
}

void queue_flush( queue_t *queue )
{
	queue->read_pos = 0;
	queue->write_pos = 0;
}

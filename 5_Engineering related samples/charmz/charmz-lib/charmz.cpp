
#include "./config/config.h"
#include "charmz.h"
#include "modules/radio.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

// Power meter and control
power_manager_t charmz::power_manager = { 0.0f, e_battery_unknown };

// Indicators
rgb_led charmz::rgb = {};
q_led charmz::qled = {};

// Controllers
charmz_state charmz::state = e_system_off;
attitude_control_t charmz::stabilizer = {};

// Sensors
imu_sensor charmz::imu = {};

// Sensor fusion
axes_angle charmz::angle = { 0.0 };
axes_angle_rate charmz::angle_rate = { 0.0 };

// Control parameters
axes_angle charmz::desired_angle = { 0.0 };
axes_angle_rate charmz::desired_angle_rate = { 0.0 };
float charmz::total_thrust = { 0.0 };

// Threads
os_thread_t charmz::m_control_thread = {};
os_thread_t charmz::m_indication_thread = {};

os_thread_t charmz::m_mavlink_receiver_thread = {};
os_thread_t charmz::m_mavlink_transceiver_thread = {};
os_thread_t charmz::m_mavlink_main_thread = {};

// Queue
os_queue_t charmz::m_status_queue = {};

// Semaphores
//os_semaphore_t charmz::m_complete_semaphore = {};

// Communication
mavcomm_t charmz::m_mavlink = {};

#define TRACE_PORT Serial

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

charmz::charmz()
{
	is_init = false;
}

charmz::~charmz()
{
	is_init = false;
}

void charmz::init()
{
	TRACE_PORT.printf( "%s(...):%d\r\n",__FUNCTION__,__LINE__ );

	if(is_init) return;

	// Motors
	motor_init();

	// Power meter and control
	power_manager_init( &power_manager );

	// Indicators
	rgb_led_init( &rgb );
	rgb.color = e_non;
	q_led_init( &qled );

	// Sensors
	imu_sensor_init( &imu );

	// Sensor fusion
	//altimeter_init( &altimetry );
	//motion_tracker_init( &motion, MOTION_UPDATE_PERIOD );
	sensor_fusion_init( SENSOR_FUSION_UPDATE_PERIOD );

	// Controllers
	state = e_system_off;
	attitude_init( &stabilizer, ATTITUDE_UPDATE_PERIOD );

	// Communication
	mavlink_init( &m_mavlink );
	parameter_init();

	TRACE_PORT.begin(115200);

	// Queue
	os_queue_create(&m_status_queue,sizeof(charmz_state),16);

	// Semaphore
	//os_semaphore_create(&m_complete_semaphore,10,0);

	// Threads
	os_thread_create( &m_control_thread,"control",2,(os_thread_fn_t)control_loop,NULL,1024 );
	os_thread_create( &m_indication_thread,"indication",3,(os_thread_fn_t)indication_loop,NULL,1024 );

	// os_thread_create( &m_mavlink_receiver_thread,"mavlink_receiver",3,(os_thread_fn_t)mavlink_receiver_loop,NULL,1024 );
	os_thread_create( &m_mavlink_transceiver_thread,"mavlink_transceiver_",3,(os_thread_fn_t)mavlink_transceiver_loop,NULL,1024 );
	os_thread_create( &m_mavlink_main_thread,"mavlink_main",3,(os_thread_fn_t)mavlink_main_loop,NULL,1024 );

	is_init = true;
}

void charmz::on()
{
	init();
	if( state == e_system_off ){
		state = e_system_on;

		TRACE_PORT.printf( "%s(...):%d\r\n",__FUNCTION__,__LINE__ );
		//spp_trace( "q_copter::power on!. \r\n" );
	}
}

void charmz::off()
{
	if( state != e_system_off ){
		// Threads
//		os_thread_cleanup( &m_control_thread );
//		os_thread_cleanup( &m_indication_thread );
		//
//		os_thread_cleanup( &m_mavlink_transceiver_thread );
//		os_thread_cleanup( &m_mavlink_main_thread );
		state = e_system_off;
	}
}

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

//
void charmz::control_loop()
{
	charmz_state p_state = e_system_off;
	//
	uint32_t tick = 0;
	coroutine_control_t power_measurement;
	coroutine_control_t imu_sensor_fusion;
	coroutine_control_t attitude_control;
	//
	COROUTINE_SUSPEND(power_measurement);
	COROUTINE_SUSPEND(imu_sensor_fusion);
	COROUTINE_SUSPEND(attitude_control);
	//
	system_tick_t previous_wake_time = millis();
	//
	for(;;) {
		os_thread_delay_until( &previous_wake_time, (system_tick_t)CONTROL_TASK_INTERVAL );
		//------------------------------------------------------------
		// Control state machine
		//------------------------------------------------------------
		switch(state){
			case e_system_on:
				// Enable power manager
				power_manager_enable( &power_manager );
				COROUTINE_RESUME( power_measurement );
				state = e_check_power;
				break;
			case e_check_power:
				// Check power status
				if( power_manager.state == e_battery_low ){
					// ToDo down counter for wait 15sec
					state = e_critical_low_power;
				} else if( power_manager.state == e_battery_charging ){
					state = e_charging_battery;
				} else if( power_manager.state == e_battery_good ){
					// Power-on radio
					radio.on();
					radio.connect();
					state = e_check_radio;
				}
				break;
			case e_charging_battery:
				// Charging battery
				charge_battery( &power_manager );
				if( power_manager.state == e_battery_good ){
					state = e_check_power;
				}
				break;
			case e_check_radio:
				if( radio.ready() ){
					TRACE_PORT.printf( "IP: %d.%d.%d.%d \r\n", WiFi.localIP()[0],WiFi.localIP()[1],WiFi.localIP()[2],WiFi.localIP()[3] );
					// Start the UDP link
					radio_link.begin( RADIO_PORT );
					// Enable mavlink
					m_mavlink.enable = true;
					state = e_check_mavlink;
				}
				break;
			case e_check_mavlink:
				if ( m_mavlink.alive ){
					state = e_find_imu_bias;
				}
				break;
			case e_find_imu_bias:
				imu_sensor_acquisition( &imu );
				if( imu.gyro_bias.is_bias_found ){
					TRACE_PORT.printf( "IMU ID:%d\r\n",imu.id );
					TRACE_PORT.printf( "IMU BIAS:%d %d %d \r\n",
							imu.gyro_bias.bias.x,imu.gyro_bias.bias.y,imu.gyro_bias.bias.z );
					state = e_ready;
					COROUTINE_RESUME( imu_sensor_fusion );
				}
				break;
			case e_ready:
				if ( m_mavlink.connection_timeout == MAV_CONNECTION_LOST ){
					COROUTINE_SUSPEND(imu_sensor_fusion);
					state = e_check_mavlink;
				} else if ( m_mavlink.system_mode == MAV_MODE_MANUAL_ARMED ){
					attitude_reset( &stabilizer );
					COROUTINE_RESUME( attitude_control );
					state = e_active;
				}
				break;
			case e_active:
				if ( m_mavlink.connection_timeout == MAV_CONNECTION_LOST ){
					COROUTINE_SUSPEND(attitude_control);
					COROUTINE_SUSPEND(imu_sensor_fusion);
					motor_update( 0, 0, 0, 0 );
					state = e_check_mavlink;
				} else if ( m_mavlink.system_mode == MAV_MODE_MANUAL_DISARMED ){
					COROUTINE_SUSPEND(attitude_control);
					motor_update( 0, 0, 0, 0 );
					state = e_ready;
				}
				break;
			case e_take_off:
				break;
			case e_hovering:
				break;
			case e_landing:
				break;
			case e_critical_low_power:
				break;
			case e_system_off:
				break;
			default: break;
		}
		//------------------------------------------------------------
		if( COROUTINE_TIME_SCALER( power_measurement,tick,INTERVAL_SCALE_128 ) ){
			// Acquisition rate 1.953125Hz T = 640ms
			power_manager_update( &power_manager );
			m_mavlink.system_status.voltage_battery = power_manager.battery_voltage * 1000;
		}
		//------------------------------------------------------------
		if( COROUTINE_ASSERT( imu_sensor_fusion ) ){
			// Acquisition rate 200Hz T = 5ms
			imu_sensor_acquisition( &imu );
			// Update rate 200Hz T = 5ms
			sensor_fusion_update( imu.gyro_out.x, imu.gyro_out.y, imu.gyro_out.z,
								  imu.accel_out.x, imu.accel_out.y, imu.accel_out.z );
			sensor_fusion_get_angle( &angle );
			//
			angle_rate.pitch = imu.gyro_out.x;
			angle_rate.roll  = imu.gyro_out.y;
			angle_rate.yaw 	 = imu.gyro_out.z;

#if 0 // For trace angle
			if( ( tick & INTERVAL_SCALE_32 ) == INTERVAL_SCALE_32 ){
				//TRACE_PORT.printf( "IMU ID:%x\r\n",imu.id );
				//TRACE_PORT.printf( "IMU GYRO SENSOR:%.3f %.3f %.3f \r\n",imu.gyro_out.x,imu.gyro_out.y, imu.gyro_out.z );
				//TRACE_PORT.printf( "IMU ACCEL SENSOR:%.3f %.3f %.3f \r\n",imu.accel_out.x,imu.accel_out.y, imu.accel_out.z );
				//TRACE_PORT.printf( "IMU ROLL:%.3f PITCH:%.3f YAW:%.3f \r\n",angle.roll, angle.pitch, angle.yaw );
			}
#endif
		}
		//------------------------------------------------------------
		if( COROUTINE_ASSERT( attitude_control ) ){
			//
#ifdef SHIELD_V2
			axes_angle t_angle;
			t_angle.pitch = RADIAN_TO_DEGREE( angle.pitch );
			t_angle.roll  = RADIAN_TO_DEGREE( angle.roll  );
			t_angle.yaw   = RADIAN_TO_DEGREE( angle.yaw   );
			//
			attitude_correct( &stabilizer, t_angle, angle_rate, desired_angle, desired_angle_rate );
#else //SHIELD_V3
			attitude_correct( &stabilizer, angle, angle_rate, desired_angle, desired_angle_rate );
#endif
			power_distribute();
		}
		//------------------------------------------------------------
		if( p_state != state ){
			p_state = state;
			os_queue_put( m_status_queue,&p_state, 0);
		}
		//------------------------------------------------------------
		tick++;
	}
}

//
void charmz::indication_loop()
{
	charmz_state p_state = e_system_off;

	for(;;) {
		if( !os_queue_take( m_status_queue,&p_state, INDICATION_TASK_INTERVAL) ){
			switch(p_state){
				case e_system_on:
					rgb.pwm.period = 1000/INDICATION_TASK_INTERVAL;
					rgb.pwm.duty_cycle = 100;
					rgb.color = e_red;
					break;
				case e_check_power:
					rgb.pwm.period = 1000/INDICATION_TASK_INTERVAL;
					rgb.pwm.duty_cycle = 30;
					rgb.color = e_red;
					break;
				case e_charging_battery:
					rgb.pwm.period = 1000/INDICATION_TASK_INTERVAL;
					rgb.pwm.duty_cycle = 50;
					rgb.color = e_red;
					break;
				case e_check_radio:
					rgb.color = e_non;
					break;
				case e_check_mavlink:
					rgb.pwm.period = 200/INDICATION_TASK_INTERVAL;
					rgb.pwm.duty_cycle = 50;
					rgb.color = e_magenta;
					break;
				case e_find_imu_bias:
					rgb.pwm.period = 160/INDICATION_TASK_INTERVAL;
					rgb.pwm.duty_cycle = 60;
					rgb.color = e_green;
					break;
				case e_ready:
					rgb.pwm.duty_cycle = 100;
					rgb.color = e_green;
					break;
				case e_active:
					rgb.pwm.duty_cycle = 100;
					rgb.color = e_blue;
					break;
				case e_take_off:
					//rgb.pwm.period = 100/INDICATION_TASK_INTERVAL;
					//rgb.pwm.duty_cycle = 70;
					break;
				case e_hovering:
					//rgb.pwm.duty_cycle = 100;
					break;
				case e_landing:
					//rgb.pwm.duty_cycle = 70;
					break;
				case e_critical_low_power:
					rgb.pwm.period = 300/INDICATION_TASK_INTERVAL;	// 2sec
					rgb.pwm.duty_cycle = 50;
					rgb.color = e_red;
					break;
				case e_system_off:
					rgb.pwm.duty_cycle = 0;
					rgb.color = e_non;
					break;
				default: break;
			}
		}
		rgb_led_update( &rgb );
		q_led_uptate( &qled );
	}
}
//
void charmz::mavlink_transceiver_loop(void)
{
	IPAddress remoteIP(192, 168, 2, 102);
	int port = 14550;
	uint8_t udp_buff[256];
	uint16_t udp_buff_size;
	//
	system_tick_t previous_wake_time = millis();
	//
	for(;;) {
		os_thread_delay_until( &previous_wake_time, (system_tick_t)COMMUNICATION_TASK_INTERVAL );
		// Check if data has been received
		if ( radio_link.parsePacket() > 0 ) {
			// Read first char of data received
			udp_buff_size = radio_link.parsePacket();
			radio_link.read(udp_buff,udp_buff_size);
			// Push the data into receive queue of mavlink
			mavlink_receive_queue_push( &m_mavlink, udp_buff, udp_buff_size );
#if 0
			// Store sender ip and port
			IPAddress ipAddress = radio_link.remoteIP();
			int dport = radio_link.remotePort();
			// Packet received
			TRACE_PORT.printf("Sender IP=%d:%d:%d:%d Port:%d Received data length:%d \r\n",
					ipAddress[0],ipAddress[1],ipAddress[2],ipAddress[3],dport,udp_buff_size );
#endif
		}
		// Ignore other chars
		// radio_link.flush();
		udp_buff_size = mavlink_transmit_queue_pull( &m_mavlink, udp_buff );
		if( udp_buff_size > 0 ){
			udp_buff_size = radio_link.sendPacket(udp_buff, udp_buff_size, remoteIP, port);
#if 0
			// Packet received
			TRACE_PORT.printf("Receiver IP=%d:%d:%d:%d Port:%d Send data length:%d \r\n",
					remoteIP[0],remoteIP[1],remoteIP[2],remoteIP[3],port,udp_buff_size );
#endif
		}
	}
}
//
void charmz::power_distribute(){
	float roll_torque, pitch_torque, yaw_torque;
	// Get stabilizer output
	attitude_get_output( &stabilizer, &roll_torque, &pitch_torque, &yaw_torque );

	//roll_torque = 0;
	//pitch_torque = 0;
	//yaw_torque = 0;

	// Get controller output
	//altitude_get_output( &controller, &total_thrust );
	//	Tau_Phi   *= Ixx;
	//	Tau_Theta *= Iyy;
	//	Tau_Psi   *= Izz;
	//controller.output = 0;
	//			float w1 = Tz/(4*k) - Tau_Theta/(2*k*l) - Tau_Phi/(2*k*l) - Tau_Psi/(4*b);
	float w1 = total_thrust/4.0 + roll_torque/2.0 + pitch_torque/2.0 - yaw_torque/4.0;
	//			float w2 = Tz/(4*k) - Tau_Theta/(2*k*l) - Tau_Phi/(2*k*l) + Tau_Psi/(4*b);
	float w2 = total_thrust/4.0 + roll_torque/2.0 - pitch_torque/2.0 + yaw_torque/4.0;
	//			float w3 = Tz/(4*k) + Tau_Theta/(2*k*l) + Tau_Phi/(2*k*l) - Tau_Psi/(4*b);
	float w3 = total_thrust/4.0 - roll_torque/2.0 + pitch_torque/2.0 + yaw_torque/4.0;
	//			float w4 = Tz/(4*k) + Tau_Theta/(2*k*l) - Tau_Phi/(2*k*l) + Tau_Psi/(4*b);
	float w4 = total_thrust/4.0 - roll_torque/2.0 - pitch_torque/2.0 - yaw_torque/4.0;
	// Distribute motor power
	motor_update( (int16_t)w1, (int16_t)w2, (int16_t)w3, (int16_t)w4 );
}
//
static mavlink_message_t msg;
//
void charmz::mavlink_main_loop(void){
	//
	uint32_t tick = 0;
	coroutine_control_t heartbeat_publisher;
	coroutine_control_t system_status_publisher;
	coroutine_control_t attitude_publisher;
	coroutine_control_t parameter_publisher;
	//
	COROUTINE_RESUME(heartbeat_publisher);
	COROUTINE_SUSPEND(system_status_publisher);
	COROUTINE_SUSPEND(attitude_publisher);
	COROUTINE_SUSPEND(parameter_publisher);
	//
	system_tick_t previous_wake_time = millis();
	//
	for(;;) {
		os_thread_delay_until( &previous_wake_time, (system_tick_t)COMMUNICATION_TASK_INTERVAL );
		// Is enabled T = 400ms
		if ( m_mavlink.enable && COROUTINE_TIME_SCALER( heartbeat_publisher,tick,INTERVAL_SCALE_8 ) ){
			// Send Heartbeat
			m_mavlink.mavlink_system.compid = MAV_COMP_ID_ALL;
			mavlink_msg_heartbeat_pack( m_mavlink.mavlink_system.sysid,
									m_mavlink.mavlink_system.compid, &msg,
									m_mavlink.system_type,
									m_mavlink.autopilot_type,
									m_mavlink.system_mode,
									m_mavlink.custom_mode,
									m_mavlink.system_state );
			mavlink_transmit_queue_push( &m_mavlink, &msg );
		}
		if ( COROUTINE_TIME_SCALER( system_status_publisher,tick,INTERVAL_SCALE_16 ) ){
			// Send Status
			//m_mavlink.system_status.voltage_battery = (uint16_t)(3.55*1000);
			mavlink_msg_sys_status_pack( m_mavlink.mavlink_system.sysid, MAV_COMP_ID_SYSTEM_CONTROL, &msg,
									m_mavlink.system_status.onboard_control_sensors_present,
									m_mavlink.system_status.onboard_control_sensors_enabled,
									m_mavlink.system_status.onboard_control_sensors_health,
									m_mavlink.system_status.load,
									m_mavlink.system_status.voltage_battery, -1, -1, 0, 0, 0, 0, 0, 0 );
			mavlink_transmit_queue_push( &m_mavlink, &msg );
		}
		if ( COROUTINE_TIME_SCALER( attitude_publisher,tick,INTERVAL_SCALE_4 ) ){
			// Send attitude
			uint32_t timeStamp = (uint32_t)millis();
			mavlink_msg_attitude_pack( m_mavlink.mavlink_system.sysid,MAV_COMP_ID_IMU, &msg, timeStamp,
									angle.roll,angle.pitch,-angle.yaw,
									angle_rate.pitch, angle_rate.roll, angle_rate.yaw);
			mavlink_transmit_queue_push( &m_mavlink, &msg );
			//TRACE_PORT.printf( "IMU ROLL:%.3f PITCH:%.3f YAW:%.3f \r\n",angle.roll, angle.pitch, angle.yaw );
		}
		if ( COROUTINE_TIME_SCALER(parameter_publisher,tick,INTERVAL_SCALE_2 ) ){
			// Send parameters one by one
			if (m_mavlink.parameter_i < m_mavlink.global_data.pointer){
				mavlink_parameter_publish( &m_mavlink, m_mavlink.parameter_i++ );
//				TRACE_PORT.printf("pointer:%d parameter_i:%d\r\n",m_mavlink.global_data.pointer,m_mavlink.parameter_i);
			} else {
				COROUTINE_SUSPEND( parameter_publisher );
				COROUTINE_RESUME( attitude_publisher );
			}
		}
		//
		uint8_t t_char;
		mavlink_status_t status;
		// Check receive queue
		while( mavlink_receive_queue_pull( &m_mavlink, &t_char ) ){
			if ( mavlink_parse_char(MAVLINK_COMM_0, t_char, &msg, &status )){
				m_mavlink.connection_timeout = MAV_CONNECTION_ALIVE;

				//TRACE_PORT.printf("msg.msgid:%d \r\n",msg.msgid);

				// Handle message
				switch ( msg.msgid ){
					case MAVLINK_MSG_ID_HEARTBEAT:
					{
						if( !m_mavlink.alive ){
							m_mavlink.alive = true;
							COROUTINE_RESUME( system_status_publisher );
							COROUTINE_RESUME( parameter_publisher );
						}
//						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_HEARTBEAT] \r\n",msg.msgid);
						break;
					}
					case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
					{
						uint16_t index = mavlink_msg_param_request_read_get_param_index( &msg );
						mavlink_parameter_publish( &m_mavlink, index );
						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_PARAM_REQUEST_READ] %d\r\n",msg.msgid,index);
						break;
					}
					case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
					{
						m_mavlink.parameter_i = 0;
						COROUTINE_SUSPEND( attitude_publisher );
						COROUTINE_RESUME( parameter_publisher );
						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_PARAM_REQUEST_LIST] \r\n",msg.msgid);
						break;
					}
					case MAVLINK_MSG_ID_PARAM_SET:
					{
						mavlink_param_set_t param_set;
						mavlink_msg_param_set_decode( &msg, &param_set );
						int index = mavlink_parameter_update( &m_mavlink, param_set.param_id, param_set.param_value );
						parameter_update( index, param_set.param_value );
						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_PARAM_SET] value:%f\r\n",msg.msgid, param_set.param_value);
						break;
					}
					case MAVLINK_MSG_ID_SET_MODE:
					{
						mavlink_set_mode_t mode;
						mavlink_msg_set_mode_decode( &msg, &mode );
						m_mavlink.system_mode = mode.base_mode;
//						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_SET_MODE] \r\n",msg.msgid);
						break;
					}
					case MAVLINK_MSG_ID_MANUAL_CONTROL:
					{
						mavlink_manual_control_t manual_control;
						mavlink_msg_manual_control_decode( &msg, &manual_control );
						if( (manual_control.buttons & 0x04) == 0x04 ){				// key:O
							m_mavlink.system_mode = MAV_MODE_MANUAL_ARMED;
						} else if( (manual_control.buttons & 0x02) == 0x02 ){		// key:X
							m_mavlink.system_mode = MAV_MODE_MANUAL_DISARMED;
						}
//						else if((manual_control.buttons && 0x08)==0x08){		// key:Tr
//							//m_mavlink.system_mode = MAV_MODE_STABILIZE_DISARMED;
//						} else if((manual_control.buttons && 0x01)==0x01){		// key:Sq
//							//m_mavlink.system_mode = MAV_MODE_STABILIZE_ARMED;
//						}
						total_thrust = manual_control.z/1.2;
#ifdef SHIELD_V2
						desired_angle.roll  =  (float)manual_control.y/50.0;
						desired_angle.pitch = -(float)manual_control.x/50.0;
#else // SHIELD_V3
						desired_angle.roll  = DEGREE_TO_RADIAN( (float)manual_control.y/40.0 );
						desired_angle.pitch = DEGREE_TO_RADIAN(-(float)manual_control.x/40.0 );
#endif
						desired_angle.yaw  	= 0.0;

//						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_MANUAL_CONTROL] \r\n KEY:%f %f %f %f %x\r\n",msg.msgid,
//												(float)manual_control.y,
//												(float)manual_control.x,
//												(float)manual_control.r,
//												(float)manual_control.z,
//												manual_control.buttons);
//						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_MANUAL_CONTROL] \r\n",msg.msgid);
						break;
					}
					case MAVLINK_MSG_ID_SYSTEM_TIME:
//						TRACE_PORT.printf("msg.msgid:%d [MAVLINK_MSG_ID_SYSTEM_TIME] \r\n",msg.msgid);
						break;
					default:
						TRACE_PORT.printf("msg.msgid:%d [OTHERS] \r\n",msg.msgid);
						break;
				}
			}
		}
		// Check connection
		if( m_mavlink.connection_timeout > 0 ) {
			m_mavlink.connection_timeout--;
		} else if( m_mavlink.connection_timeout == 0 ) {
			m_mavlink.system_mode = MAV_MODE_MANUAL_DISARMED;
			m_mavlink.connection_timeout = MAV_CONNECTION_LOST;
			//m_mavlink.parameter_i = 0;
			m_mavlink.alive = false;
			COROUTINE_SUSPEND( system_status_publisher );
			COROUTINE_SUSPEND( attitude_publisher );
			COROUTINE_SUSPEND( parameter_publisher );
		}
		tick++;
	}
}
//
void charmz::parameter_init(void)
{
	// Registering parameters
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll.kp, 			"ROLL P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll.ki, 			"ROLL I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll.kd, 			"ROLL D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll.iLimit, 		"ROLL IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll.iLimitLow, 	"ROLL ILow");

	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll_rate.kp, 		"ROLL RATE P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll_rate.ki, 		"ROLL RATE I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll_rate.kd, 		"ROLL RATE D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll_rate.iLimit, 	"ROLL RATE IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_roll_rate.iLimitLow,"ROLL RATE ILow");

	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch.kp, 			"PITCH P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch.ki, 			"PITCH I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch.kd, 			"PITCH D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch.iLimit, 		"PITCH IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch.iLimitLow, 	"PITCH ILow");

	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch_rate.kp, 		"PITCH RATE P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch_rate.ki, 		"PITCH RATE I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch_rate.kd,		"PITCH RATE D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch_rate.iLimit,	"PITCH RATE IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_pitch_rate.iLimitLow,"PITCH RATE ILow");

	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw.kp, 			"YAW P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw.ki, 			"YAW I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw.kd, 			"YAW D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw.iLimit, 		"YAW IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw.iLimitLow,		"YAW ILow");

	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw_rate.kp, 		"YAW RATE P");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw_rate.ki, 		"YAW RATE I");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw_rate.kd, 		"YAW RATE D");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw_rate.iLimit, 	"YAW RATE IHigh");
	mavlink_parameter_register( &m_mavlink, &stabilizer.pid_yaw_rate.iLimitLow, "YAW RATE ILow");
	// Parameter initialize
//	int addr = 0;
//	for( ; addr < m_mavlink.global_data.pointer; addr++ ){
		// Read a value (2 bytes in this case) from EEPROM addres
//		float value;
//		EEPROM.get(0x1f+addr, value);
//		if ( !isinf(value) && !isnan(value) && -100.0 < value && value < 100.0 ) {
//			*m_mavlink.global_data.param[addr] = value;
//		}
//	}
}
//
void charmz::parameter_update(int addr,float value)
{
	if ( addr < m_mavlink.global_data.pointer ){
		float value = *m_mavlink.global_data.param[addr];
		EEPROM.put(0x1f+addr, value);
	}
}

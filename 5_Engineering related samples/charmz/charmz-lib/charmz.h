
#ifndef CHARMZ_H_
#define CHARMZ_H_

#include <stdint.h>
#include "application.h"

extern "C"
{
#include "drivers/led.h"
#include "drivers/motor.h"
#include "modules/power_manager.h"
#include "modules/imu_sensor.h"
#include "modules/attitude_estimation.h"
#include "modules/attitude_control.h"
#include "util/mavlink_interface/mavlink_interface.h"
#include "util/coroutine.h"
}
//#include "modules/trace.h"

// Shield status
typedef enum {
	e_system_off		 = 0b00000000,
	e_system_on			 = 0b00000001,
	e_check_power 		 = 0b00000010,
	e_charging_battery 	 = 0b00000011,
	e_check_radio 		 = 0b00000100,
	e_check_mavlink 	 = 0b00000101,
	e_check_sensors 	 = 0b00000110,
	e_find_imu_bias 	 = 0b00000111,
	e_ready				 = 0b00001000,
	e_active			 = 0b00001001,
	e_take_off			 = 0b00001010,
	e_hovering			 = 0b00001011,
	e_landing			 = 0b00001100,
	e_critical_low_power = 0b00001101
} charmz_state;

//
class charmz{

public:
	charmz(void);
	~charmz(void);

	void init();
	void on(void);
	void off(void);

	//static void set_desired( float roll, float pitch, float yaw, float thrust, int btn );
	//static void mavlink_execute();

private:

	static void control_loop(void);
	static void indication_loop(void);

	static void mavlink_receiver_loop(void);
	static void mavlink_transceiver_loop(void);
	static void mavlink_main_loop(void);

	static void power_distribute(void);
	static void parameter_service(void);
	//
	static void parameter_init(void);
	static void parameter_update(int addr,float value);

	//static void mavlink(void);
	//static void set_parameter( float *param );
	//static bool get_parameter( float *param );

//private:
public:
	bool is_init;

	// Power meter and control
	static power_manager_t power_manager;

	// Sensors

	static imu_sensor imu;
/*
	static baro_sensor baro;

	// Sensor fusion
	static altimeter altimetry;
	static motion_xyz motion;
*/
	static axes_angle angle;
	static axes_angle_rate angle_rate;

	// Indicators
	static rgb_led 	rgb;
	static q_led 	qled;

	// Controllers
	static charmz_state state;
//	static altitude_control controller;
	static attitude_control_t stabilizer;

	// Threads
	static os_thread_t m_control_thread;
	static os_thread_t m_indication_thread;

	static os_thread_t m_mavlink_receiver_thread;
	static os_thread_t m_mavlink_transceiver_thread;
	static os_thread_t m_mavlink_main_thread;

	// Queue
	static os_queue_t m_status_queue;

	// Semaphores
//	static os_semaphore_t m_complete_semaphore;

	//
	static bool power_flag;
	static bool enable_battery_gauge;
	static bool enable_imu;
	static bool enable_baro;
	static bool enable_sensor_fusion;
	static bool enable_motion_track;
	static bool enable_attitude_stabilizer;
	static bool enable_altitude_stabilizer;
	static bool enable_motor;

	// Control parameters
	static axes_angle desired_angle;
	static axes_angle_rate desired_angle_rate;
	static float total_thrust;

	// Communication
	static mavcomm_t m_mavlink;
};

#endif /* CHARMZ_H_ */

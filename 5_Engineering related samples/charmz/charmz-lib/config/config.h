
#ifndef CONFIG_H_
#define CONFIG_H_

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

// T = 5ms  F = 200Hz
#define CONTROL_TASK_INTERVAL 		5	// milliseconds
#define INDICATION_TASK_INTERVAL	50	// milliseconds
#define COMMUNICATION_TASK_INTERVAL	50	// milliseconds
#define SENSOR_FUSION_UPDATE_PERIOD ((float)CONTROL_TASK_INTERVAL/1000.0f)
#define ATTITUDE_UPDATE_PERIOD 		5 //SENSOR_FUSION_UPDATE_PERIOD
#define MOTION_UPDATE_PERIOD 		SENSOR_FUSION_UPDATE_PERIOD
#define ALTITUDE_UPDATE_PERIOD 		SENSOR_FUSION_UPDATE_PERIOD/2.0

#define SHIELD_V2
//#define SHIELD_V3

#endif /* CONFIG_H_ */

#ifndef HAL_GPIO_H_
#define HAL_GPIO_H_

#include <stdint.h>
#include <stdbool.h>
#include "pinmap_hal.h"

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

#define LOW 	false
#define HIGH	true

//
typedef PinMode pin_mode;

//
typedef enum {
        e_sample_3cycles   = ((uint8_t)0x00),
        e_sample_15cycles  = ((uint8_t)0x01),
        e_sample_28cycles  = ((uint8_t)0x02),
        e_sample_56cycles  = ((uint8_t)0x03),
        e_sample_84cycles  = ((uint8_t)0x04),
        e_sample_112cycles = ((uint8_t)0x05),
        e_sample_144cycles = ((uint8_t)0x06),
        e_sample_480cycles = ((uint8_t)0x07)
} adc_sample_time_t;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

// Digital IO setting
void gpio_pin_mode(uint16_t pin, pin_mode set_mode);

// Reads the value from a specified digital pin, either HIGH or LOW.
bool gpio_read(uint8_t pin);

// Write a HIGH or a LOW value to a digital pin.
void gpio_write(uint8_t pin, bool value);

//
void gpio_adc_dma_init();

// The function is used to change the default sample time for analog read.
void gpio_set_adc_sample_time(adc_sample_time_t adc_sample_time);

// Reads the value from the specified analog pin.
int32_t gpio_analog_read(uint16_t pin);

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* HAL_GPIO_H_*/


#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>
#include <stdbool.h>

#define NULL ((char *)0)

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

//
void i2c_set_speed(uint32_t clock_spd);

//
void i2c_init();

//
int8_t i2c_read_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data);

//
int8_t i2c_read_bytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t length, uint8_t *data);

//
bool i2c_write_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t data);

//
bool i2c_write_bytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t length, uint8_t *data);

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* HAL_I2C_H_ */

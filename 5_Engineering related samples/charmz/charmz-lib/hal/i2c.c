
#include "i2c.h"
#include "i2c_hal.h"

#include <stddef.h>

//----------------------------------------------------------------------
// Private defines  ----------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Private functions ----------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

// NOTE: should be called before i2c_init();
void i2c_set_speed(uint32_t clock_spd)
{
	HAL_I2C_Set_Speed(HAL_I2C_INTERFACE1,clock_spd,NULL);
}

//
void i2c_init()
{
	os_thread_scheduling(false, NULL);

	HAL_I2C_Init(HAL_I2C_INTERFACE1, NULL);
	HAL_I2C_Begin(HAL_I2C_INTERFACE1,I2C_MODE_MASTER,0x00,NULL);

	os_thread_scheduling(true, NULL);
}

//
int8_t i2c_read_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data)
{
	os_thread_scheduling(false, NULL);

	HAL_I2C_Begin_Transmission(HAL_I2C_INTERFACE1,dev_addr,NULL);
	HAL_I2C_Write_Data(HAL_I2C_INTERFACE1, reg_addr, NULL);
	HAL_I2C_End_Transmission(HAL_I2C_INTERFACE1,true,NULL);
	HAL_I2C_Request_Data(HAL_I2C_INTERFACE1,dev_addr,1,true,NULL);
	*data = HAL_I2C_Read_Data(HAL_I2C_INTERFACE1,NULL);

	os_thread_scheduling(true, NULL);
	return 1;
}

//
int8_t i2c_read_bytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t length, uint8_t *data)
{
	os_thread_scheduling(false, NULL);

	HAL_I2C_Begin_Transmission(HAL_I2C_INTERFACE1,dev_addr,NULL);
	HAL_I2C_Write_Data(HAL_I2C_INTERFACE1, reg_addr, NULL);
	HAL_I2C_End_Transmission(HAL_I2C_INTERFACE1,true,NULL);
			
	HAL_I2C_Request_Data(HAL_I2C_INTERFACE1,dev_addr,length,true,NULL);
	uint8_t n;
	for (n = 0;(HAL_I2C_Available_Data(HAL_I2C_INTERFACE1, NULL) > 0); n++){
		data[n] = HAL_I2C_Read_Data(HAL_I2C_INTERFACE1,NULL);
	}

	os_thread_scheduling(true, NULL);
	return n;
}

//
bool i2c_write_byte(uint8_t dev_addr, uint8_t reg_addr, uint8_t data)
{
	os_thread_scheduling(false, NULL);

	HAL_I2C_Begin_Transmission(HAL_I2C_INTERFACE1,dev_addr,NULL);
	HAL_I2C_Write_Data(HAL_I2C_INTERFACE1, reg_addr, NULL);
	uint8_t value=HAL_I2C_Write_Data(HAL_I2C_INTERFACE1, data, NULL);
	HAL_I2C_End_Transmission(HAL_I2C_INTERFACE1,true,NULL);

	os_thread_scheduling(true, NULL);
	return value==1 ? true:false;
}

//
bool i2c_write_bytes(uint8_t dev_addr, uint8_t reg_addr, uint8_t length, uint8_t* data)
{
	os_thread_scheduling(false, NULL);
	HAL_I2C_Begin_Transmission(HAL_I2C_INTERFACE1,dev_addr,NULL);
	HAL_I2C_Write_Data(HAL_I2C_INTERFACE1, reg_addr, NULL);
	uint8_t value = 0;
	for(uint8_t n = 0; n < length; n++){
	  value = HAL_I2C_Write_Data(HAL_I2C_INTERFACE1,data[n], NULL);
	}
	HAL_I2C_End_Transmission(HAL_I2C_INTERFACE1,true,NULL);

	os_thread_scheduling(true, NULL);
	return value==1 ? true:false;
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#include "gpio_hal.h"
#include "adc_hal.h"
#include "gpio.h"

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

//
void gpio_pin_mode(uint16_t pin, pin_mode mode )
{
	HAL_Pin_Mode(pin, mode);
}

//
void gpio_write(uint8_t pin, bool value)
{
	HAL_GPIO_Write(pin, value);
}

//
bool gpio_read(uint8_t pin)
{
	return HAL_GPIO_Read(pin)==1 ? HIGH:LOW;
}

//
void gpio_adc_dma_init()
{
	HAL_ADC_DMA_Init();
}

//
void gpio_set_adc_sample_time(adc_sample_time_t adc_sample_time)
{
	
	HAL_ADC_Set_Sample_Time(adc_sample_time);
}

//
int32_t gpio_analog_read(uint16_t pin)
{
	return HAL_ADC_Read(pin);
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

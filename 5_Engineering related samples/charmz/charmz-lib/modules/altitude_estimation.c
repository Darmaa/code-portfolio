#include <stdint.h>
#include <math.h>
#include "altitude_estimation.h"

#include "../util/filter.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

// Constants used to determine altitude from pressure
#define SEA_PRESSURE 	(float)1013.25f
#define PRESSURE_FACTOR (float)0.190284f
#define PRESSURE_NORM	(float)145366.45f

// Pressure to altitude function fitted by parabola
#define COFF_A (float)0.0133092704054512f
#define COFF_B (float)-54.0701454115402f
#define COFF_C (float)41124.9953055234f

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

// Butterworth lowpass filter ( Fcutoff 1.0Hz ; Fs = 100Hz )
const float lowpass1_coff_a[] = { 1.0000000000000000f, -0.969067417193793f };
const float lowpass1_coff_b[] = { 0.0154662914031034f, 0.0154662914031034f };

// Butterworth lowpass filter ( Fcutoff 0.5Hz ; Fs = 100Hz )
const float lowpass2_coff_a[] = { 1.0000000000000000f, -0.984414127416097f };
const float lowpass2_coff_b[] = { 0.00779293629195155f, 0.00779293629195155f };

// Butterworth lowpass filter ( Fcutoff 0.05Hz; Fs = 100Hz )
const float lowpass3_coff_a[] = { 1.00000000000000000f, -0.998430436083094f };
const float lowpass3_coff_b[] = { 0.00078478195845294f, 0.00078478195845294f };

// Butterworth highpass filter ( Fcutoff 0.5Hz; Fs = 200Hz )
const float highpass_coff_a[] = { 1.000000000000000f, -0.992176700177507f };
const float highpass_coff_b[] = { 0.996088350088753f, -0.996088350088753f };

static iir_filter low_pass1 = {};
static iir_filter low_pass2 = {};
static iir_filter low_pass3 = {};
static uint8_t is_filter_init = 0;

static iir_filter high_pass1 = {};
static iir_filter high_pass2 = {};
static iir_filter high_pass3 = {};

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

float pressure_to_altitude(float pressure, float temperature)
{
	return ( COFF_A*pressure*pressure + COFF_B*pressure + COFF_C );
}

// Deadzone
static float deadband(float value, const float threshold)
{
	if (fabs(value) < threshold){
		value = 0;
	} else if (value > 0) {
		value -= threshold;
	} else if (value < 0) {
		value += threshold;
	}
	return value;
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void altimeter_init( altimeter *altitude )
{
	altitude->raw = 0.0;
	altitude->smoot1 = 0.0;
	altitude->smoot2 = 0.0;
	altitude->smoot3 = 0.0;
	altitude->desired = 0.0;
}

void altimeter_update( altimeter *altitude, float pressure, float temperature )
{
	if( pressure > 0 ){
		float raw_altitude = pressure_to_altitude( pressure, temperature);
		if( is_filter_init ){
			altitude->smoot1 = iir_filtering( raw_altitude, &low_pass1 );
			altitude->smoot2 = iir_filtering( raw_altitude, &low_pass2 );
			altitude->smoot3 = iir_filtering( raw_altitude, &low_pass3 );
		} else {
			is_filter_init = 1;
			altitude->smoot1 = raw_altitude;
			altitude->smoot2 = raw_altitude;
			altitude->smoot3 = raw_altitude;

			iir_init( &low_pass1, lowpass1_coff_a, lowpass1_coff_b, raw_altitude );
			iir_init( &low_pass2, lowpass2_coff_a, lowpass2_coff_b, raw_altitude );
			iir_init( &low_pass3, lowpass3_coff_a, lowpass3_coff_b, raw_altitude );
		}
		altitude->raw = raw_altitude;
	}
}

void motion_tracker_init( motion_xyz *tracker, const float period )
{
	tracker->acceleration.x = 0.0;
	tracker->acceleration.y = 0.0;
	tracker->acceleration.y = 0.0;

	tracker->velocity.x = 0.0;
	tracker->velocity.y = 0.0;
	tracker->velocity.z = 0.0;

	tracker->dt = period;

	iir_init( &high_pass1, highpass_coff_a, highpass_coff_b, 0.0 );
	iir_init( &high_pass2, highpass_coff_a, highpass_coff_b, 0.0 );
	iir_init( &high_pass3, highpass_coff_a, highpass_coff_b, 0.0 );
//	imu_bias_init( &tracker->accel_bias );
}

void motion_tracker_update( motion_xyz *tracker, float_xyz *acceleration, bool update_velocity )
{
//	imu_add_bias(&tracker->accel_bias, acceleration);
//
//	if( !tracker->accel_bias.is_bias_found ){
//		imu_find_bias(&tracker->accel_bias);
//	}

	// High-pass filter linear acceleration to remove offset
//	tracker->acceleration.x = iir_filtering( ( acceleration->x - tracker->accel_bias.bias.x ) * 9.81, &high_pass1 );
//	tracker->acceleration.y = iir_filtering( ( acceleration->y - tracker->accel_bias.bias.y ) * 9.81, &high_pass2 );
//	tracker->acceleration.z = iir_filtering( ( acceleration->z - tracker->accel_bias.bias.z ) * 9.81, &high_pass3 );

	tracker->acceleration.x = iir_filtering( ( acceleration->x ) * 9.81, &high_pass1 );
	tracker->acceleration.y = iir_filtering( ( acceleration->y ) * 9.81, &high_pass2 );
	tracker->acceleration.z = iir_filtering( ( acceleration->z ) * 9.81, &high_pass3 );

	if( update_velocity ){
		tracker->velocity.x += tracker->dt * tracker->acceleration.x;
		tracker->velocity.y += tracker->dt * tracker->acceleration.y;
		tracker->velocity.z += tracker->dt * tracker->acceleration.z;

		tracker->velocity.x = deadband( tracker->velocity.x, 0.001 );
		tracker->velocity.y = deadband( tracker->velocity.y, 0.001 );
		tracker->velocity.z = deadband( tracker->velocity.z, 0.001 );

		//tracker->velocity_raw.x = iir_filtering( tracker->velocity.x, &high_pass1 );
		//tracker->velocity_raw.y = iir_filtering( tracker->velocity.y, &high_pass2 );
		//tracker->velocity_raw.z = iir_filtering( tracker->velocity.z, &high_pass3 );
	}
}

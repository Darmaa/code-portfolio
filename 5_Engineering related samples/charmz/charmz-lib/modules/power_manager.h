
#ifndef POWER_MANAGER_H_
#define POWER_MANAGER_H_

#include <stdint.h>
#include <stdbool.h>

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

typedef enum power_state {
	e_battery_unknown = 0,
	e_battery_good,
	e_battery_low,
	e_battery_charging
} power_state_t;

typedef struct {
	float battery_voltage;
	power_state_t state;
	bool enable;
} power_manager_t;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

//
void power_manager_init( power_manager_t *power );
void power_manager_enable( power_manager_t *power );
void power_manager_disable( power_manager_t *power );
void power_manager_update( power_manager_t *power );
void charge_battery( power_manager_t *power );
void system_off(void);

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* POWER_MANAGER_H_ */

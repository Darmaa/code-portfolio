
#ifndef ATTITUDE_ESTIMATION_H_
#define ATTITUDE_ESTIMATION_H_

#include <math.h>

#define M_PI_FLOAT ((float) M_PI)
#define RADIAN_TO_DEGREE( rad ) ( (rad)*180.0/M_PI_FLOAT )
#define DEGREE_TO_RADIAN( deg ) ( (deg)*M_PI_FLOAT/180.0 )

typedef struct {
	float roll;
	float pitch;
	float yaw;
} axes_angle;

typedef struct {
	float roll;
	float pitch;
	float yaw;
} axes_angle_rate;

void sensor_fusion_init( const float period );
void sensor_fusion_update( float gx, float gy, float gz, float ax, float ay, float az );
void sensor_fusion_get_angle( axes_angle *angle );
void sensor_fusion_tilt_compensate( float ax, float ay, float az, float *cax, float *cay, float *caz );

#endif /* ATTITUDE_ESTIMATION_H_ */

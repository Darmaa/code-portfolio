
#ifndef IMU_SENSOR_H_
#define IMU_SENSOR_H_

#include <stdint.h>
#include <stdbool.h>

#define IMU_BIAS_NUMBER_OF_SAMPLES  128

typedef struct {
	int16_t x;
	int16_t y;
	int16_t z;
} int16_xyz;

typedef struct {
	int32_t x;
	int32_t y;
	int32_t z;
} int32_xyz;

typedef struct {
	int64_t x;
	int64_t y;
	int64_t z;
} int64_xyz;

typedef struct {
	float x;
	float y;
	float z;
} float_xyz;

typedef struct{
	int16_xyz   bias;
	bool		is_bias_found;
	bool       	is_buffer_filled;
	int16_xyz*  pointer;
	int16_xyz   buffer[IMU_BIAS_NUMBER_OF_SAMPLES];
} bias_xyz;

typedef struct {
	bias_xyz	gyro_bias;
	float_xyz	gyro_out; 	// Gyro axis data in deg/s
	float_xyz	accel_out; 	// Accelerometer axis data in mG
	uint8_t 	id;
} imu_sensor;

void imu_sensor_init( imu_sensor *sensor );
void imu_sensor_acquisition( imu_sensor *sensor );

#endif /* IMU_SENSOR_H_ */


#include "../drivers/mpu9250.h"
#include "imu_sensor.h"

#include <math.h>

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

#define IMU_GNORM (double)(0.0076*M_PI/180.0) 	// degree to radian
#define IMU_ANORM (double)(0.000061035)			//6.1035e-05
#define IMU_MNORM (double)(0.006)				//(0.05)//1.5e-02

#define GYRO_NUMBER_OF_AXES	3
#define GYRO_VARIANCE_BASE        8000*8
#define GYRO_VARIANCE_THRESHOLD_X (GYRO_VARIANCE_BASE)
#define GYRO_VARIANCE_THRESHOLD_Y (GYRO_VARIANCE_BASE)
#define GYRO_VARIANCE_THRESHOLD_Z (GYRO_VARIANCE_BASE)

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

void imu_calculate_mean(bias_xyz* bias, int32_xyz* mean_out)
{
  uint32_t i;
  int32_xyz sum = {0};

  for (i = 0; i < IMU_BIAS_NUMBER_OF_SAMPLES; i++){
	  sum.x += bias->buffer[i].x;
	  sum.y += bias->buffer[i].y;
	  sum.z += bias->buffer[i].z;
  }
  mean_out->x = sum.x / IMU_BIAS_NUMBER_OF_SAMPLES;
  mean_out->y = sum.y / IMU_BIAS_NUMBER_OF_SAMPLES;
  mean_out->z = sum.z / IMU_BIAS_NUMBER_OF_SAMPLES;
}

void imu_calculate_variance_and_mean(bias_xyz* bias, int32_xyz* var_out, int32_xyz* mean_out)
{
  uint32_t i;
  int32_xyz sum = { .x = 0, .y = 0, .z = 0 };
  int64_xyz sum_square = {0};

  for (i = 0; i < IMU_BIAS_NUMBER_OF_SAMPLES; i++){
	sum.x += bias->buffer[i].x;
    sum.y += bias->buffer[i].y;
    sum.z += bias->buffer[i].z;
    sum_square.x += bias->buffer[i].x * bias->buffer[i].x;
    sum_square.y += bias->buffer[i].y * bias->buffer[i].y;
    sum_square.z += bias->buffer[i].z * bias->buffer[i].z;
  }

  var_out->x = (sum_square.x - ((int64_t)sum.x * (int64_t)sum.x) / IMU_BIAS_NUMBER_OF_SAMPLES);
  var_out->y = (sum_square.y - ((int64_t)sum.y * (int64_t)sum.y) / IMU_BIAS_NUMBER_OF_SAMPLES);
  var_out->z = (sum_square.z - ((int64_t)sum.z * (int64_t)sum.z) / IMU_BIAS_NUMBER_OF_SAMPLES);

  mean_out->x = sum.x / IMU_BIAS_NUMBER_OF_SAMPLES;
  mean_out->y = sum.y / IMU_BIAS_NUMBER_OF_SAMPLES;
  mean_out->z = sum.z / IMU_BIAS_NUMBER_OF_SAMPLES;
}

bool imu_find_bias(bias_xyz* bias)
{
  if (bias->is_buffer_filled){
    int32_xyz variance;
    int32_xyz mean;

    imu_calculate_variance_and_mean(bias, &variance, &mean);

    if (variance.x < GYRO_VARIANCE_THRESHOLD_X &&
        variance.y < GYRO_VARIANCE_THRESHOLD_Y &&
        variance.z < GYRO_VARIANCE_THRESHOLD_Z )
    {
      bias->bias.x = mean.x;
      bias->bias.y = mean.y;
      bias->bias.z = mean.z;
      bias->is_bias_found = true;
      return true;
    }
  }
  return false;
}

void imu_add_bias(bias_xyz* bias, int16_xyz* value)
{
  bias->pointer->x = value->x;
  bias->pointer->y = value->y;
  bias->pointer->z = value->z;
  bias->pointer++;

  if( bias->pointer >= &bias->buffer[IMU_BIAS_NUMBER_OF_SAMPLES] ){
      bias->pointer = bias->buffer;
      bias->is_buffer_filled = true;
  }
}

void imu_bias_init(bias_xyz* bias)
{
	bias->is_buffer_filled = false;
	bias->pointer = bias->buffer;
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void imu_sensor_init( imu_sensor* sensor )
{
	mpu_init();
	imu_bias_init( &sensor->gyro_bias );

	sensor->id = mpu_get_id();
}

void imu_sensor_acquisition( imu_sensor* sensor )
{
	int16_xyz accel;
	int16_xyz gyro;

	mpu_get_imu6( (int16_t*)&accel, (int16_t*)&gyro );

	imu_add_bias(&sensor->gyro_bias, &gyro);

	if (!sensor->gyro_bias.is_bias_found){
	   imu_find_bias(&sensor->gyro_bias);
	}

	sensor->gyro_out.x = (gyro.x - sensor->gyro_bias.bias.x) * IMU_GNORM;
	sensor->gyro_out.y = (gyro.y - sensor->gyro_bias.bias.y) * IMU_GNORM;
	sensor->gyro_out.z = (gyro.z - sensor->gyro_bias.bias.z) * IMU_GNORM;

	sensor->accel_out.x = accel.x * IMU_ANORM;
	sensor->accel_out.y = accel.y * IMU_ANORM;
	sensor->accel_out.z = accel.z * IMU_ANORM;
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

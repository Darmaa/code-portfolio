
#ifndef ALTITUDEESTIMATION_H_
#define ALTITUDEESTIMATION_H_

#include "imu_sensor.h"
#include "attitude_estimation.h"

#define ACC_BIAS_NUMBER_OF_SAMPLES 64

//typedef struct{
//	xyz_float   bias;
//	bool		is_bias_found;
//	bool       	is_buffer_filled;
//	xyz_float*  pointer;
//	xyz_float   buffer[ACC_BIAS_NUMBER_OF_SAMPLES];
//} xyz_float_bias;

typedef struct {
	float raw;
	float smoot1;
	float smoot2;
	float smoot3;
	float desired;
} altimeter;

typedef struct {
//	xyz_float_bias accel_bias;
	float_xyz acceleration;
	float_xyz velocity;
	float dt;
} motion_xyz;

void altimeter_init( altimeter *altitude );
void altimeter_update( altimeter *altitude, float pressure, float temperature );

void motion_tracker_init( motion_xyz *tracker, const float period );
void motion_tracker_update( motion_xyz *tracker, float_xyz *linear_acceleration, bool update_velocity );

#endif /* Q_SHIELD_LIB_LIBRARIES_ALTITUDEESTIMATION_H_ */

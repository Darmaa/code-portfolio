
#include "power_manager.h"

#include "../drivers/bq24075.h"
#include "../drivers/max16054.h"
#include "../util/filter.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

#define THRESHOLD_BATTERY_GOOD	((float)3.45f) 

#define BATTERY_VOLTAGE_NORM 	((float)0.002302511f) //0.002416992
#define T_2N7002_VDS			((float)0.07f) //

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

// Butterworth lowpass filter ( Fcutoff 0.1Hz ; Fs = 1.5625Hz )
const float filter_coff_a[] = {(float)1.00000000f, (float)-0.66138369f};
const float filter_coff_b[] = {(float)0.16930816f, (float)0.16930816f};


static iir_filter low_pass = {};

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

void power_pin_init()
{
	gpio_pin_mode(SYS_OFF, OUTPUT);

	gpio_pin_mode(EN2, OUTPUT);
	gpio_pin_mode(EN1, OUTPUT);

	gpio_pin_mode(PGOOD,INPUT_PULLUP);
	gpio_pin_mode(CHG,	INPUT_PULLUP);

	//gpio_pin_mode(VBAT, AN_INPUT);
	gpio_analog_read(VBAT);
	gpio_write(SYS_OFF, LOW);
	gpio_write(EN2, LOW);
	gpio_write(EN1, LOW);
	
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

//
void power_manager_init( power_manager_t *power )
{
	power_pin_init();
	power->state = e_battery_unknown;
	power->battery_voltage = gpio_analog_read(VBAT) * BATTERY_VOLTAGE_NORM + T_2N7002_VDS;
	power->enable = false;
	iir_init( &low_pass, filter_coff_a, filter_coff_b, power->battery_voltage );
}

//
void power_manager_enable( power_manager_t *power )
{
	power->enable = true;
}

//
void power_manager_disable( power_manager_t *power )
{
	power->state = e_battery_unknown;
	power->enable = false;
}

//
void power_manager_update( power_manager_t *power )
{
	if( !power->enable ) return;

	uint8_t is_chg = !gpio_read(CHG);
	uint8_t is_pgood = !gpio_read(PGOOD);

	float voltage = gpio_analog_read(VBAT) * BATTERY_VOLTAGE_NORM + T_2N7002_VDS;
	power->battery_voltage = iir_filtering( voltage, &low_pass );

	if( is_pgood && is_chg ){
		gpio_write(EN1, HIGH);
		power->state = e_battery_charging;
	} else if( power->battery_voltage < THRESHOLD_BATTERY_GOOD ) {
		power->state = e_battery_low;
	} else if( power->battery_voltage > THRESHOLD_BATTERY_GOOD ) {
		power->state = e_battery_good;
	}
}

//
void charge_battery( power_manager_t *power )
{
	uint8_t is_chg = !gpio_read(CHG);
	uint8_t is_pgood = !gpio_read(PGOOD);

	switch(power->state){
		case e_battery_charging:
			if( !is_pgood || !is_chg ){
				gpio_write(EN1, LOW);
				power->state = e_battery_good;
			}
			break;
		case e_battery_good: break;
		case e_battery_low:	 break;
		default: break;
	}
}

//
void system_off()
{
	gpio_write(SYS_OFF, HIGH);
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

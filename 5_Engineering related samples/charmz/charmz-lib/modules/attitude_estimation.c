#include "attitude_estimation.h"

//----------------------------------------------------------------------
// Private variables ---------------------------------------------------
//----------------------------------------------------------------------

// Variable definitions
static float dt = 1.0;
static float two_kp = (2.0f * 0.4f);	// 2 * proportional gain
static float two_ki = (2.0f * 0.001f); 	// 2 * integral gain
static float integral_x = 0.0f;
static float integral_y = 0.0f;
static float integral_z = 0.0f;	// integral error terms scaled by ki

static float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f; // quaternion of sensor frame relative to auxiliary frame

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root
float inverse_of_square_root( float x ){
	unsigned long i = 0x5F1F1412 - (*(unsigned long*)&x >> 1);
	float tmp = *(float*)&i;
	return tmp * (1.69000231f - 0.714158168f * x * tmp * tmp);
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void sensor_fusion_init( const float period ){
	q0 = 1.0f;
	q1 = 0.0f;
	q2 = 0.0f;
	q3 = 0.0f;

	integral_x = 0.0f;
	integral_y = 0.0f;
	integral_z = 0.0f;

	dt = period;
}

void sensor_fusion_update( float gx, float gy, float gz, float ax, float ay, float az ){
  float recipNorm;
  float halfvx, halfvy, halfvz;
  float halfex, halfey, halfez;
  float qa, qb, qc;

  // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
  if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))){
    // Normalise accelerometer measurement
    recipNorm = inverse_of_square_root(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Estimated direction of gravity and vector perpendicular to magnetic flux
    halfvx = q1 * q3 - q0 * q2;
    halfvy = q0 * q1 + q2 * q3;
    halfvz = q0 * q0 - 0.5f + q3 * q3;

    // Error is sum of cross product between estimated and measured direction of gravity
    halfex = (ay * halfvz - az * halfvy);
    halfey = (az * halfvx - ax * halfvz);
    halfez = (ax * halfvy - ay * halfvx);

    // Compute and apply integral feedback if enabled
    if(two_ki > 0.0f){
    	integral_x += two_ki * halfex * dt;  // integral error scaled by Ki
    	integral_y += two_ki * halfey * dt;
    	integral_z += two_ki * halfez * dt;
    	gx += integral_x;  // apply integral feedback
    	gy += integral_y;
    	gz += integral_z;
    } else {
    	integral_x = 0.0f; // prevent integral windup
    	integral_y = 0.0f;
    	integral_z = 0.0f;
    }
	// Apply proportional feedback
	gx += two_kp * halfex;
	gy += two_kp * halfey;
	gz += two_kp * halfez;
  }
  // Integrate rate of change of quaternion
  gx *= (0.5f * dt);   // pre-multiply common factors
  gy *= (0.5f * dt);
  gz *= (0.5f * dt);
  qa = q0;
  qb = q1;
  qc = q2;
  q0 += (-qb * gx - qc * gy - q3 * gz);
  q1 += (qa * gx + qc * gz - q3 * gy);
  q2 += (qa * gy - qb * gz + q3 * gx);
  q3 += (qa * gz + qb * gy - qc * gx);

  // Normalize quaternion
  recipNorm = inverse_of_square_root( q0*q0 + q1*q1 + q2*q2 + q3*q3 );
  q0 *= recipNorm;
  q1 *= recipNorm;
  q2 *= recipNorm;
  q3 *= recipNorm;
}

void sensor_fusion_get_angle( axes_angle *angle ){
  // estimated gravity direction
  float gx = 2 * (q1*q3 - q0*q2);
  float gy = 2 * (q0*q1 + q2*q3);
  float gz = q0*q0 - q1*q1 - q2*q2 + q3*q3;

  if (gx>1){  gx=1; }
  if (gx<-1){ gx=-1;}

  angle->yaw 	= atan2(2*(q0*q3 + q1*q2), q0*q0 + q1*q1 - q2*q2 - q3*q3);
  angle->pitch 	= atan2(gy, gz);
  angle->roll 	=-asin(gx);
}

void sensor_fusion_tilt_compensate( float ax, float ay, float az, float *cax, float *cay, float *caz )
{
	// QUATERN2ROTMAT Converts a quaternion orientation to a rotation matrix
	// Converts a quaternion orientation to a rotation matrix.
	// For more information see:
	// http://www.x-io.co.uk/node/8#quaternions
	// Date          Author          Notes
	// 27/09/2011    SOH Madgwick    Initial release

 	float R_1_1 = 2*q0*q0 - 1 + 2*q1*q1;
 	float R_1_2 = 2*(q1*q2 + q0*q3);
 	float R_1_3 = 2*(q1*q3 - q0*q2);
 	float R_2_1 = 2*(q1*q2 - q0*q3);
 	float R_2_2 = 2*q0*q0 - 1 + 2*q2*q2;
 	float R_2_3 = 2*(q2*q3 + q0*q1);
 	float R_3_1 = 2*(q1*q3 + q0*q2);
 	float R_3_2 = 2*(q2*q3 - q0*q1);
 	float R_3_3 = 2*q0*q0 - 1 + 2*q3*q3;

 	// Calculate 'tilt-compensated' accelerometer
 	// transpose R matrix because ahrs provides Earth relative to sensor
 	*cax = R_1_1*ax + R_2_1*ay + R_3_1*az;
 	*cay = R_1_2*ax + R_2_2*ay + R_3_2*az;
 	*caz = R_1_3*ax + R_2_3*ay + R_3_3*az - 1.0;
 	// cax, cay, caz accelerometer in Earth frame
}

#include "attitude_control.h"
#include "../config/config.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

#ifdef SHIELD_V2

	#define DEFAULT_ROLL_RATE_KP 10.0  // 10.0
	#define DEFAULT_ROLL_RATE_KI 0.0
	#define DEFAULT_ROLL_RATE_KD 0.0
	#define DEFAULT_ROLL_RATE_INTEGRATION_LIMIT 0.0

	#define DEFAULT_PITCH_RATE_KP 10.0 // 10.0
	#define DEFAULT_PITCH_RATE_KI 0.0
	#define DEFAULT_PITCH_RATE_KD 0.0
	#define DEFAULT_PITCH_RATE_INTEGRATION_LIMIT 0.0

	#define DEFAULT_YAW_RATE_KP 10.0 	// 30.0 // 2.0 //5.0
	#define DEFAULT_YAW_RATE_KI 5.0 	// 10.0  // 1.0
	#define DEFAULT_YAW_RATE_KD 0.0
	#define DEFAULT_YAW_RATE_INTEGRATION_LIMIT 10.0

	#define DEFAULT_ROLL_KP 0.1 //2 //0.15  // 0.8
	#define DEFAULT_ROLL_KI 0.01 //0.05
	#define DEFAULT_ROLL_KD 0.0
	#define DEFAULT_ROLL_INTEGRATION_LIMIT 5.0	//15

	#define DEFAULT_PITCH_KP 0.1 //2 // 0.15	// 0.8
	#define DEFAULT_PITCH_KI 0.01 //0.05
	#define DEFAULT_PITCH_KD 0.0
	#define DEFAULT_PITCH_INTEGRATION_LIMIT 5.0 //15

	#define DEFAULT_YAW_KP 0.04	//0.0
	#define DEFAULT_YAW_KI 0.0
	#define DEFAULT_YAW_KD 0.0
	#define DEFAULT_YAW_INTEGRATION_LIMIT 0.0

#else //SHIELD_V3

	#define DEFAULT_ROLL_RATE_KP 10.0
	#define DEFAULT_ROLL_RATE_KI 0.0
	#define DEFAULT_ROLL_RATE_KD 0.0
	#define DEFAULT_ROLL_RATE_INTEGRATION_LIMIT 1.0

	#define DEFAULT_PITCH_RATE_KP 10.0
	#define DEFAULT_PITCH_RATE_KI 0.0
	#define DEFAULT_PITCH_RATE_KD 0.0
	#define DEFAULT_PITCH_RATE_INTEGRATION_LIMIT 1.0

	#define DEFAULT_YAW_RATE_KP 10.0
	#define DEFAULT_YAW_RATE_KI 0.0
	#define DEFAULT_YAW_RATE_KD 0.0
	#define DEFAULT_YAW_RATE_INTEGRATION_LIMIT 1.0

	#define DEFAULT_ROLL_KP 0.5 		// 0.1
	#define DEFAULT_ROLL_KI 0.25 		// 0.01
	#define DEFAULT_ROLL_KD 0.01
	#define DEFAULT_ROLL_INTEGRATION_LIMIT 3.0

	#define DEFAULT_PITCH_KP 0.5 		// 0.1
	#define DEFAULT_PITCH_KI 0.25 		// 0.01
	#define DEFAULT_PITCH_KD 0.01
	#define DEFAULT_PITCH_INTEGRATION_LIMIT 3.0

	#define DEFAULT_YAW_KP 0.1			// 0.04
	#define DEFAULT_YAW_KI 0.0
	#define DEFAULT_YAW_KD 0.0
	#define DEFAULT_YAW_INTEGRATION_LIMIT 3.0

#endif

	#define DEFAULT_ROLL_OUTPUT_MIN  (INT8_MIN/2.0)
	#define DEFAULT_ROLL_OUTPUT_MAX  (INT8_MAX/2.0)
	#define DEFAULT_PITCH_OUTPUT_MIN (INT8_MIN/2.0)
	#define DEFAULT_PITCH_OUTPUT_MAX (INT8_MAX/2.0)
	#define DEFAULT_YAW_OUTPUT_MIN   (INT8_MIN/2.0)
	#define DEFAULT_YAW_OUTPUT_MAX   (INT8_MAX/2.0)

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------
#undef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#undef min
#define min(a,b) ((a) < (b) ? (a) : (b))

float constrain_float(float value, const float minval, const float maxval)
{
	return min(maxval, max(minval,value));
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void attitude_init( attitude_control_t *controller, const float period )
{
	pid_init( &controller->pid_roll, 0, DEFAULT_ROLL_KP, DEFAULT_ROLL_KI, DEFAULT_ROLL_KD, 	period );
	pid_init( &controller->pid_pitch,0, DEFAULT_PITCH_KP,DEFAULT_PITCH_KI,DEFAULT_PITCH_KD, period );
	pid_init( &controller->pid_yaw,	 0, DEFAULT_YAW_KP,  DEFAULT_YAW_KI,  DEFAULT_YAW_KD,   period );

	pid_init( &controller->pid_roll_rate, 	0, DEFAULT_ROLL_RATE_KP, DEFAULT_ROLL_RATE_KI, DEFAULT_ROLL_RATE_KD,	period );
	pid_init( &controller->pid_pitch_rate,	0, DEFAULT_PITCH_RATE_KP,DEFAULT_PITCH_RATE_KI,DEFAULT_PITCH_RATE_KD,	period );
	pid_init( &controller->pid_yaw_rate,	0, DEFAULT_YAW_RATE_KP,  DEFAULT_YAW_RATE_KI,  DEFAULT_YAW_RATE_KD,		period );

	pid_set_integral_limit( &controller->pid_roll,  DEFAULT_ROLL_INTEGRATION_LIMIT,	-DEFAULT_ROLL_INTEGRATION_LIMIT  );
	pid_set_integral_limit( &controller->pid_pitch, DEFAULT_PITCH_INTEGRATION_LIMIT,-DEFAULT_PITCH_INTEGRATION_LIMIT );
	pid_set_integral_limit( &controller->pid_yaw,   DEFAULT_YAW_INTEGRATION_LIMIT,	-DEFAULT_YAW_INTEGRATION_LIMIT   );

	pid_set_integral_limit( &controller->pid_roll_rate,  DEFAULT_ROLL_RATE_INTEGRATION_LIMIT,	-DEFAULT_ROLL_RATE_INTEGRATION_LIMIT  );
	pid_set_integral_limit( &controller->pid_pitch_rate, DEFAULT_PITCH_RATE_INTEGRATION_LIMIT,	-DEFAULT_PITCH_RATE_INTEGRATION_LIMIT );
	pid_set_integral_limit( &controller->pid_yaw_rate,   DEFAULT_YAW_RATE_INTEGRATION_LIMIT,	-DEFAULT_YAW_RATE_INTEGRATION_LIMIT	  );

	controller->roll_output  = 0;
	controller->pitch_output = 0;
	controller->yaw_output   = 0;
}

void attitude_correct( attitude_control_t *controller,
		const axes_angle actual_angle, const axes_angle_rate actual_angle_rate,
		const axes_angle desired_angle,const axes_angle_rate desired_angle_rate )
{
	float t_pid;
	// Update PID for roll axis
	pid_set_desired( &controller->pid_roll, desired_angle.roll );
	t_pid = pid_update( &controller->pid_roll, actual_angle.roll, true );

	// Update PID for roll rate
	pid_set_desired( &controller->pid_roll_rate, t_pid + desired_angle_rate.roll );
	controller->roll_output = pid_update( &controller->pid_roll_rate, actual_angle_rate.roll, true );

	// Update PID for pitch axis
	pid_set_desired( &controller->pid_pitch, desired_angle.pitch );
	t_pid = pid_update( &controller->pid_pitch, actual_angle.pitch, true );

	// Update PID for pitch rate
	pid_set_desired( &controller->pid_pitch_rate, t_pid + desired_angle_rate.pitch );
	controller->pitch_output = pid_update( &controller->pid_pitch_rate, actual_angle_rate.pitch, true );

	// Update PID for yaw axis
	float yaw_error = desired_angle.yaw - actual_angle.yaw;

	pid_set_error( &controller->pid_yaw, yaw_error );
	t_pid = pid_update( &controller->pid_yaw, actual_angle.yaw, false);

	pid_set_desired( &controller->pid_yaw_rate, t_pid + desired_angle_rate.yaw );
	controller->yaw_output = pid_update( &controller->pid_yaw_rate, actual_angle_rate.yaw, true );
}

void attitude_reset( attitude_control_t *controller )
{
	pid_reset( &controller->pid_roll 		);
	pid_reset( &controller->pid_pitch 		);
	pid_reset( &controller->pid_yaw 		);
	pid_reset( &controller->pid_roll_rate 	);
	pid_reset( &controller->pid_pitch_rate 	);
	pid_reset( &controller->pid_yaw_rate 	);
}

void attitude_get_output( attitude_control_t *controller, float *roll_output, float *pitch_output, float *yaw_output )
{
	*roll_output	= constrain_float( controller->roll_output,	DEFAULT_ROLL_OUTPUT_MIN, DEFAULT_ROLL_OUTPUT_MAX	);
	*pitch_output	= constrain_float( controller->pitch_output,DEFAULT_PITCH_OUTPUT_MIN, DEFAULT_PITCH_OUTPUT_MAX 	);
	*yaw_output		= constrain_float( controller->yaw_output,  DEFAULT_YAW_OUTPUT_MIN, DEFAULT_YAW_OUTPUT_MAX 	 	);
}

void attitude_set_parameter( attitude_control_t *controller, float *param )
{
	controller->pid_roll.kp 			= param[0];
	controller->pid_roll.ki 			= param[1];
	controller->pid_roll.kd 			= param[2];
	controller->pid_roll.iLimit 		= param[3];
	controller->pid_roll.iLimitLow 		= param[4];

	controller->pid_roll_rate.kp		= param[5];
	controller->pid_roll_rate.ki		= param[6];
	controller->pid_roll_rate.kd		= param[7];
	controller->pid_roll_rate.iLimit	= param[8];
	controller->pid_roll_rate.iLimitLow = param[9];

	controller->pid_pitch.kp 			= param[10];
	controller->pid_pitch.ki 			= param[11];
	controller->pid_pitch.kd 			= param[12];
	controller->pid_pitch.iLimit 		= param[13];
	controller->pid_pitch.iLimitLow 	= param[14];

	controller->pid_pitch_rate.kp		= param[15];
	controller->pid_pitch_rate.ki		= param[16];
	controller->pid_pitch_rate.kd		= param[17];
	controller->pid_pitch_rate.iLimit 	= param[18];
	controller->pid_pitch_rate.iLimitLow= param[19];

	controller->pid_yaw.kp 				= param[20];
	controller->pid_yaw.ki 				= param[21];
	controller->pid_yaw.kd 				= param[22];
	controller->pid_yaw.iLimit			= param[23];
	controller->pid_yaw.iLimitLow		= param[24];

	controller->pid_yaw_rate.kp 		= param[25];
	controller->pid_yaw_rate.ki			= param[26];
	controller->pid_yaw_rate.kd			= param[27];
	controller->pid_yaw_rate.iLimit		= param[28];
	controller->pid_yaw_rate.iLimitLow	= param[29];
}

void attitude_get_parameter( attitude_control_t *controller, float *param )
{
	param[0]  = controller->pid_roll.kp;
	param[1]  = controller->pid_roll.ki;
	param[2]  = controller->pid_roll.kd;
	param[3]  = controller->pid_roll.iLimit;
	param[4]  = controller->pid_roll.iLimitLow;

	param[5]  = controller->pid_roll_rate.kp;
	param[6]  = controller->pid_roll_rate.ki;
	param[7]  = controller->pid_roll_rate.kd;
	param[8]  = controller->pid_roll_rate.iLimit;
	param[9]  = controller->pid_roll_rate.iLimitLow;

	param[10] = controller->pid_pitch.kp;
	param[11] = controller->pid_pitch.ki;
	param[12] = controller->pid_pitch.kd;
	param[13] = controller->pid_pitch.iLimit;
	param[14] = controller->pid_pitch.iLimitLow;

	param[15] = controller->pid_pitch_rate.kp;
	param[16] = controller->pid_pitch_rate.ki;
	param[17] = controller->pid_pitch_rate.kd;
	param[18] = controller->pid_pitch_rate.iLimit;
	param[19] = controller->pid_pitch_rate.iLimitLow;

	param[20] = controller->pid_yaw.kp;
	param[21] = controller->pid_yaw.ki;
	param[22] = controller->pid_yaw.kd;
	param[23] = controller->pid_yaw.iLimit;
	param[24] = controller->pid_yaw.iLimitLow;

	param[25] = controller->pid_yaw_rate.kp;
	param[26] = controller->pid_yaw_rate.ki;
	param[27] = controller->pid_yaw_rate.kd;
	param[28] = controller->pid_yaw_rate.iLimit;
	param[29] = controller->pid_yaw_rate.iLimitLow;
}

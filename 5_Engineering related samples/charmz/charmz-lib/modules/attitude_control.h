#ifndef attitude_control_t_H_
#define attitude_control_t_H_

#include "../util/pid.h"
#include "attitude_estimation.h"

typedef struct {
	pid pid_roll;
	pid pid_pitch;
	pid pid_yaw;

	pid pid_roll_rate;
	pid pid_pitch_rate;
	pid pid_yaw_rate;

	float roll_output;
	float pitch_output;
	float yaw_output;
} attitude_control_t;

void attitude_init( attitude_control_t *controller, const float period );
void attitude_reset( attitude_control_t *controller );
void attitude_correct( attitude_control_t *controller,
					const axes_angle actual_angle,  const axes_angle_rate actual_angle_rate,
					const axes_angle desired_angle, const axes_angle_rate desired_angle_rate );
void attitude_get_output( attitude_control_t *controller, float* roll_output, float* pitch_output, float* yaw_output);
void attitude_set_parameter( attitude_control_t *controller, float *param );
void attitude_get_parameter( attitude_control_t *controller, float *param );

#endif /* attitude_control_t_H_ */


#ifndef BQ24075_H_
#define BQ24075_H_

#include "../hal/gpio.h"

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

#define EN2		D7
#define EN1		D6

#define PGOOD	D5
#define CHG		D4

#define VBAT    A2

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* BQ24075_H_ */

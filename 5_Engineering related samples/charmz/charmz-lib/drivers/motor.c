#include "../hal/gpio.h"
#include "../hal/pwm.h"
#include "motor.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

#define MOTOR_1	A4
#define MOTOR_2	A5
//#define MOTOR_3	A6
//#define MOTOR_4	A7
#define MOTOR_3	RX
#define MOTOR_4	WKP

#undef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#undef min
#define min(a,b) ((a) < (b) ? (a) : (b))

//----------------------------------------------------------------------
// Private functions ---------------------------------------------------
//----------------------------------------------------------------------

uint16_t constrain(int16_t value, const int16_t minval, const int16_t maxval)
{
	return min(maxval, max(minval,value));
}

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

void motor_init()
{
	A6_disconnect();

	pwm_init( MOTOR_1 );
	pwm_init( MOTOR_2 );
	pwm_init( MOTOR_3 );
	pwm_init( MOTOR_4 );
}

void motor_update( int16_t m1, int16_t m2, int16_t m3, int16_t m4 )
{
	pwm_set( MOTOR_1, constrain(m1,0,UINT8_MAX) );
	pwm_set( MOTOR_2, constrain(m2,0,UINT8_MAX) );
	pwm_set( MOTOR_3, constrain(m3,0,UINT8_MAX) );
	pwm_set( MOTOR_4, constrain(m4,0,UINT8_MAX) );
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

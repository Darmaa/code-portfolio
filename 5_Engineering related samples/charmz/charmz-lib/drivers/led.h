
#ifndef DRIVERS_LED_H_
#define DRIVERS_LED_H_

#include <stdint.h>

//----------------------------------------------------------------------
// Exported types ------------------------------------------------------
//----------------------------------------------------------------------

typedef enum {
        e_non = 0,
        e_red = 1,
        e_green,
        e_yellow,
        e_blue,
        e_magenta,
        e_cyan,
        e_white
} rgb_color;

typedef struct {
        uint8_t duty_cycle;
        uint16_t period;
        uint16_t counter;
} soft_pwm;

typedef struct {
        soft_pwm pwm;
        uint8_t color;
} rgb_led;

typedef struct {
        soft_pwm pwm;
} q_led;

//----------------------------------------------------------------------
// Function prototypes -------------------------------------------------
//----------------------------------------------------------------------

void q_led_init( q_led *led );
void q_led_uptate( q_led *led );

void rgb_led_init( rgb_led *led );
void rgb_led_update( rgb_led *led );

//----------------------------------------------------------------------
//----------------------------------------------------------------------

#endif /* DRIVERS_LED_LED_H_ */

#include "../hal/i2c.h"
#include "mpu9250.h"

// MPU6500 related
// define here are used minimum style.
// defined what only used.
#define MPU6500_ADDRESS_AD0_LOW     0x68 // address pin low (GND), default for InvenSense evaluation board
#define MPU6500_ADDRESS_AD0_HIGH    0x69 // address pin high (VCC)
#define MPU6500_DEFAULT_ADDRESS     MPU6500_ADDRESS_AD0_HIGH
#define MPU6500_RA_WHO_AM_I         0x75
#define MPU6500_RA_PWR_MGMT_1		0x6B
#define MPU6500_RA_CONFIG			0x1A
#define MPU6500_RA_SMPLRT_DIV 		0x19
#define MPU6500_RA_GYRO_CONFIG 		0x1B
#define MPU6500_RA_ACCEL_CONFIG 	0x1C
#define MPU6500_RA_ACCEL_CONFIG2 	0x1D
#define MPU6500_RA_INT_PIN_CFG		0x37
#define MPU6500_RA_INT_ENABLE		0x38
#define MPU6500_RA_ACCEL_XOUT_H		0x3B

// AK8963 magneto related
#define AK8963_XOUT_L	0x03
#define AK8963_ADDRESS	0x0C
#define AK8963_CNTL		0x0A
#define AK8963_CNTL2	0x0B
#define AK8963_ASAX		0x10
#define AK8963_ST1		0x02

#define IMU_DEV_ADDR  MPU6500_DEFAULT_ADDRESS
#define IMU_I2C_SPEED (uint32_t)400000



void wait_cycle(uint32_t cycle)
{
	while( (cycle--) > 0 )
	{}
}

void mpu_init()
{
	i2c_set_speed(IMU_I2C_SPEED);
	i2c_init();

	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_PWR_MGMT_1, 0x80);
	wait_cycle( 100000 );

	i2c_write_byte(IMU_DEV_ADDR,MPU6500_RA_CONFIG, 0x03);

	// Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
	// Use a 200 Hz rate; a rate consistent with the filter update rate
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_SMPLRT_DIV , 0x04);

	// Set gyroscope full scale range
	// Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
	uint8_t c;
	i2c_read_byte(IMU_DEV_ADDR, MPU6500_RA_GYRO_CONFIG, &c);
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_GYRO_CONFIG, c & ~0x02); // Clear Fchoice bits [1:0]
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_GYRO_CONFIG, c & ~0x18); // Clear AFS bits [4:3]
	//i2c_write_byte(imu_dev_addr, MPU6050_RA_GYRO_CONFIG, c | 0 << 3); // Set full scale range for the gyro
	//i2c_write_byte(imu_dev_addr, MPU6050_RA_GYRO_CONFIG, c | 0x03);

	// Set accelerometer full scale range configuration
	i2c_read_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG, &c);
	// writeRegister(MPU6050_RA_ACCEL_CONFIG, c & ~0xE0); // Clear self-test bits [7:5]
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG, c & ~0x18); // Clear AFS bits [4:3]
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG, c | 0 << 3); // Set full scale range for the accelerometer

	// Set accelerometer sample rate configuration
	// It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
	// accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
	i2c_read_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG2, &c);
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG2, c & ~0x0F); // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
	//i2c_write_byte(imu_dev_addr, MPU6050_RA_ACCEL_CONFIG2, c | 0x03); // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_ACCEL_CONFIG2, c | 0x02); // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
	wait_cycle( 100000 );

	// The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
	// but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting
	// Configure Interrupts and Bypass Enable
	// Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
	// clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
	// can join the I2C bus and all can be controlled by the Arduino as master
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_INT_PIN_CFG, 0x22);
	i2c_write_byte(IMU_DEV_ADDR, MPU6500_RA_INT_ENABLE, 0x01); // Enable data ready (bit 0) interrupt
	wait_cycle( 100000 );
}
/*
// AK8963
void mag_init()
{
	 // First extract the factory calibration for each magnetometer axis
	uint8_t rawData[3]; // x/y/z gyro calibration data stored here
	i2c_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x00); // Power down magnetometer
	temp_delay(100);
	i2c_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x0F); // Enter Fuse ROM access mode
	temp_delay(100);

	i2c_write_byte(AK8963_ADDRESS, AK8963_CNTL, 0x00); // Power down magnetometer
	temp_delay(100);
	// Configure the magnetometer for continuous read and highest resolution
	// set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
	// and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
	i2c_write_byte(AK8963_ADDRESS, AK8963_CNTL, 1 << 4 | 0x06); // Set magnetometer data resolution and sample ODR
	temp_delay(1000);
}
*/

void mpu_get_imu6(  int16_t* accel, int16_t* gyro  )
{
	uint8_t buffer[14];
	if (i2c_read_bytes(IMU_DEV_ADDR, MPU6500_RA_ACCEL_XOUT_H, 14, buffer) == 14) {
	//if( mpu_read_bytes(0x69, MPU6500_RA_ACCEL_XOUT_H, 14, buffer, TIME_OUT) == 14 ){
		//fixme the following code needs to be examined. causes a interrupt to NMI_VECTOR
		// Typecasting recheck compiler was showing error without bracelet
		accel[0] = (int16_t)((buffer[0] << 8) | buffer[1] );
		accel[1] = (int16_t)((buffer[2] << 8) | buffer[3] );
		accel[2] = (int16_t)((buffer[4] << 8) | buffer[5] );

		gyro[0] = (int16_t)((buffer[8] << 8) | buffer[9] );
		gyro[1] = (int16_t)((buffer[10]<< 8) | buffer[11]);
		gyro[2] = (int16_t)((buffer[12]<< 8) | buffer[13]);
	}
}

// TODO
void mpu_reset()
{
}

void mag_reset()
{
	i2c_write_byte(AK8963_ADDRESS, AK8963_CNTL2, 1);
	wait_cycle( 300000 );
}

uint8_t mpu_get_id()
{
	uint8_t ret = 0;
	i2c_read_byte(IMU_DEV_ADDR, MPU6500_RA_WHO_AM_I, &ret);
	return ret;
}

uint8_t is_i2c_enabled()
{
	return i2c_is_enabled();
}

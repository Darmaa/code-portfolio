
#include "../hal/gpio.h"
#include "led.h"

//----------------------------------------------------------------------
// Private defines -----------------------------------------------------
//----------------------------------------------------------------------

// RGB LEDs
#define RGB_R	A1
#define RGB_G	D2
#define RGB_B	A0
// Q LEDs
#define Q_LED	D3

//----------------------------------------------------------------------
// Public functions ----------------------------------------------------
//----------------------------------------------------------------------

//
void q_led_init( q_led *led )
{
	gpio_pin_mode(Q_LED, OUTPUT);

	gpio_write(Q_LED, LOW);

	led->pwm.counter = 0;
	led->pwm.period = 0;
	led->pwm.duty_cycle = 0;
}

//
void q_led_uptate( q_led *led )
{
	if( ++led->pwm.counter >= led->pwm.period ){
		led->pwm.counter = 0;
	}
	if( led->pwm.counter < (led->pwm.duty_cycle*led->pwm.period)/100 ){
		gpio_write(Q_LED, HIGH);
	} else {
		gpio_write(Q_LED, LOW);
	}
}

//
void rgb_led_init( rgb_led *led )
{
	gpio_pin_mode(RGB_R,OUTPUT);
	gpio_pin_mode(RGB_G,OUTPUT);
	gpio_pin_mode(RGB_B,OUTPUT);

	gpio_write(RGB_R,LOW);
	gpio_write(RGB_G,LOW);
	gpio_write(RGB_B,LOW);

	led->pwm.counter = 0;
	led->pwm.period = 0;
	led->pwm.duty_cycle = 0;
	led->color = e_non;
}

//
void rgb_led_update( rgb_led *led )
{
	if( ++led->pwm.counter >= led->pwm.period ){
		led->pwm.counter = 0;
	}
	if( led->pwm.counter < (led->pwm.duty_cycle*led->pwm.period)/100 ){
		if( led->color & e_red )
			gpio_write(RGB_R, HIGH);
		else
			gpio_write(RGB_R,LOW);

		if( led->color & e_green )
			gpio_write(RGB_G, HIGH);
		else
			gpio_write(RGB_G,LOW);

		if( led->color & e_blue )
			gpio_write(RGB_B, HIGH);
		else
			gpio_write(RGB_B,LOW);
	} else {
		gpio_write(RGB_R,LOW);
		gpio_write(RGB_G,LOW);
		gpio_write(RGB_B,LOW);
	}
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

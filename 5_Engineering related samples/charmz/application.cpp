/* Includes ------------------------------------------------------------------*/  
//#include "application.h"
#include "stdarg.h"

#include "charmz-lib/charmz.h"

PRODUCT_ID(PLATFORM_ID);
PRODUCT_VERSION(2);

#if 0
	SYSTEM_MODE(AUTOMATIC);
#else
	SYSTEM_MODE(MANUAL);
#endif
	SYSTEM_THREAD(ENABLED);

/* Function prototypes -------------------------------------------------------*/

// Create class instance for charmz
charmz my_charm;

/* This function is called once at start up ----------------------------------*/
void setup()
{
    // Configure wifi
	Particle.disconnect();
    // Power-On charmz shield
    my_charm.on();
}

/* This function is called once at start up ----------------------------------*/
void loop()
{
	//Serial.println("Hello Computer");
	delay(100);
	//Wire.beginTransmission(0x69); // transmit to slave device
	//Wire.endTransmission();    // stop transmitting
}


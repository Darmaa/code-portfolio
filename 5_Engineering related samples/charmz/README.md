# README #

# Checkout charmz firmware
* cd ~/photon/firmware/user/applications
* git clone git@bitbucket.org:gl-firmware/charmz.git

# The Build Firmware
* cd ~/photon/firmware/main
* export PLATFORM=photon
* make APP=charmz

# Updating System Firmware (Photon)
* make APP=charmz program-dfu
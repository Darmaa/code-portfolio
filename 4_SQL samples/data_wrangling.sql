SELECT
    col_a
   ,col_b
  ,CASE angilal
    WHEN 'val' THEN TRIM(RIGHT(name, LENGTH(name)-1))
    ELSE name
   END AS name_clean
  ,reg_date
  ,org_register_numerical
FROM
  `project-id.dataset_name.table_name`
WHERE cat is not null and country_name is not null
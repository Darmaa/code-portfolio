SELECT 
    s.*
FROM `project-id.datasetA.table_a` as s
LEFT JOIN (
    SELECT 
        DISTINCT org_register
    FROM `project-id.datasetB.table_b`
    WHERE org_register > 0 ) as t
ON s.org_register_numerical = t.org_register
WHERE t.org_register is not null
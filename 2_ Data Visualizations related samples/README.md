## Brief intro ##

In this directory, two visualizations visualized on Tableau and Google DataStudio has been placed. 

### 1. Tableau  ###

[This](https://public.tableau.com/app/profile/darambazar.amgalan/viz/Storyboardingassignment/TheStory) Tableau visualization is the final assignment of Coursera's [Data Visualization with Tableau specialization](https://www.coursera.org/specializations/data-visualization), multiple courses. The task was to build a data story around retail consumer company's sales along with metrics. Click here for the [Certificate](https://coursera.org/share/1140a4bfa74d928781c504b66ef94aaf) of the Specializations.

![Tableau screenshot](https://github.com/Darma/code-portolio/blob/main/2_%20Data%20Visualizations%20related%20samples/tableau_story.png "Table Story Board Screen Shot")

### 2. Google DataStudio  ###

Business intelligence tools are good tools for a quick and easy Exploratory Data Analysis. Refer the [eda-data-studio.pdf](https://github.com/Darma/code-portolio/blob/main/2_%20Data%20Visualizations%20related%20samples/eda-data-studio.pdf) file in this repo.
Google Bigquery was used for Cloud analytic and storage. Google Datastudio integrated very well worked with minimum effort to use straight from BigQuery. However, when the report size and requirement get complex, Datastudio performance has been affected. One of the potential solutions is to migrate to PowerBI.


![Datastudio Screenshot](https://github.com/Darma/code-portolio/blob/main/2_%20Data%20Visualizations%20related%20samples/datastudio_ss.png "Datastudio EDA Screenshot")

import os
import sys

import numpy as np
import pandas as pd
import datetime as dt

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException


def main(input:str = ""): # expacting org level URL 
    print("******************************\n\tMAIN()")
    web = WScraper()
    web.urlGet(input)
    web.getCatB()
    print("Closing")
    web.close()
    return

class WScraper:
    wURL = "https://ikon.mn/"
    winPath = './ChromeDriverForWin/chromedriver'
    macPath = './ChromeDriverForMac/chromedriver'
    linPath = '' #Currently no linux test
    baseLinePath = './scrape_result'
    OrgMetaDataCols = ["..."]
    orgMetaDataSave = baseLinePath+"/OrgMetaData.csv"

    repMetaDataCols = ["..."]
    repMetaDataSave = baseLinePath + "/..ReportMetaData.csv"

    def __init__(self):
        #super.__init__(self)
        self.br = self.selenDrive()
        self.br.set_window_size(1920, 1080 )
        #webdriver.Chrome('./ChromeDriverForMac/chromedriver')

    
    def selenDrive(self):
        os_type = sys.platform
        #os_type = 'linux'
        try:
            if (os_type == 'win32'):
                driver = webdriver.Chrome(WScraper.winPath) # On WinOS
                return driver
            elif (os_type == 'darwin'):
                driver = webdriver.Chrome(WScraper.macPath) # On WinOS
                return driver
        except ValueError:
            raise ValueError("OS type has not found") 
    
    def setup(self):
        pass

    def urlGet(self, url):
        dr = self.br
        dr.get(url)

    def getCatB(self):

        #Org Level metadata saving here.
        MetaData = self.getOrgMetaData()
        orgMD_df = pd.DataFrame([MetaData], columns=WScraper.OrgMetaDataCols)
        self.write_record(orgMD_df, WScraper.orgMetaDataSave)

        #Finding Hurungu oruulalt, tender, hudaldan avalt
        xpath = '/html/body/div[2]/div/div/div[2]/div/div[1]/div[2]/a'
        self.br.find_element_by_xpath(xpath).click()
        #Finding year option selection
        xpath = '/html/body/div[2]/div/div/div[1]/div[2]/form/div/div/select'
        all_options = self.br.find_element_by_xpath(xpath).find_elements_by_tag_name("option")
        
        #second loop, looping over year
        for opt in range(len(all_options)):
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/form/div/div/select'
            all_options = self.br.find_element_by_xpath(xpath).find_elements_by_tag_name("option")
            OptSelYear = all_options[opt].get_attribute("value") # selecting year
            all_options[opt].click()
            ReportURL = self.br.current_url
            print(OptSelYear)
            print(ReportURL)
            self.getReports()
        
        return

    def getReports(self): #Report Level metadata gathered here.
        loop_range = [3, 4, 5, 7, 8, 9, 11, 12, 13, 15, 16, 17]
        self.getB4Reports(loop_range)
        return

    def getB4Reports(self, loop_range):
        #print(loop_range)
        base_url = self.br.current_url
        for i in loop_range: #Running throught months
            #print(i)
            xpath = '/html/body/div[2]/div/div/div[2]/div/div[2]/table/tbody/tr[4]'+'/td['+ str(i) +']/a/i' #+'/td[2]' 
            element = self.br.find_element_by_xpath(xpath)
            ReportStatus = element.get_attribute("class")

        
            ReportMetaData = self.getReportMetaData(i)
            repMD_df = pd.DataFrame([ReportMetaData], columns=WScraper.repMetaDataCols)

            if (ReportStatus=='fa fa-ban') or (ReportStatus=='fa fa-minus'): # no data table to scrap
                #print("ban or minus")
                repMD_df["Reason"] = "fa fa-ban or fa fa-minus"
                pass # No data to mine
            else:
                element.click()
                # MetaData Gathering Mechanism is required
                res = self.getTableData()
                repMD_df["Reason"] = res[1]
                self.br.get(base_url)

            self.write_record(repMD_df, WScraper.repMetaDataSave)
 
        return

    def getTableData(self):
        TableURL = self.br.current_url

        xpath = '/html/body/div[2]/div/div/div[2]/div/div[2]/div[2]'
        element = self.br.find_element_by_xpath(xpath)
        isInfoCircle = element.get_attribute('innerHTML')
        # ifthere is no table check
        if 'fa fa-info-circle' in isInfoCircle:
            IsExistReason = (element.get_attribute('innerText')) # Text after circle icon
            PublishedDate = np.nan
            return False, IsExistReason
        
        xpath_orgName = '/html/body/div[2]/div/div/div[1]/div[2]/form/div/p'
        xpath_pDate = '/html/body/div[2]/div/div/div[2]/div/div[2]/div[1]/div[1]/p'
        xpath_rDate ='/html/body/div[2]/div/div/div[2]/div/div[2]/div[1]/div[1]/h4/span'
        
        try:
            OrgName = self.br.find_element_by_xpath(xpath_orgName).get_attribute('innerText')
        except:
            OrgName = np.nan
        try:
            PublishedDate = self.br.find_element_by_xpath(xpath_pDate).get_attribute('innerText')
        except:
            PublishedDate = np.nan
        try:
            ReportDate = self.br.find_element_by_xpath(xpath_rDate).get_attribute('innerText')
        except:
            ReportDate = np.nan
        
        self.brZoomOut()
        html = self.br.page_source
        table = pd.read_html(html)
        self.brZoomReset()

        df_temp = pd.DataFrame(table[0][2:]).reset_index(drop=True)
        print("table len: {} and url: {}".format(len(df_temp), TableURL))
        
        df_temp["OrgName"] = OrgName
        df_temp["ReportDate"] = ReportDate
        df_temp["PublishedDate"] = PublishedDate
        df_temp["TableURL"] = TableURL
        df_temp["RetrievedDate"] = self.getRetDate()

        rd = "".join(ReportDate.split())
        fn = WScraper.baseLinePath + "/" + 'raws' + "/b_4_" + OrgName + "_" + rd + ".csv"
        self.write_record(df_temp, fileName=fn)

        return True, "Table Data Exist"

    def close(self):
        dr = self.br
        dr.quit()

    def brZoomOut(self):
        self.br.execute_script("document.body.style.zoom='20%'")
        return
    
    def brZoomReset(self):
        self.br.execute_script("document.body.style.zoom='100%'")
        return 

    def write_record(self, df:pd.DataFrame, fileName:str):
        with open(fileName, 'a', encoding='utf-8') as f:
            df.to_csv(f, mode='a', header=f.tell()==0, index_label=False, index=False, na_rep='NaN')
        return

    def getReportMetaData(self):
        return
    
    def getRetDate(self)->str:
        return dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    
    def getReportMetaData(self, loc):
        dr = self.br
        xpath_orgName = '/html/body/div[2]/div/div/div[2]/div/div[2]/table/tbody/tr[4]/td[2]'
        xpath_repName = '/html/body/div[2]/div/div/div[1]/div[2]/form/div/p'
        xpath_repDate = '/html/body/div[2]/div/div/div[2]/div/div[2]/table/thead/tr/th[' + str(loc) + ']'
        xpath_link    = '/html/body/div[2]/div/div/div[2]/div/div[2]/table/tbody/tr[4]/td[' + str(loc) +']/a'
        xpath_class   = '/html/body/div[2]/div/div/div[2]/div/div[2]/table/tbody/tr[4]/td[' + str(loc) +']/a/i'
        
        try:
            ReportOrgName = dr.find_element_by_xpath(xpath_orgName).get_attribute("innerText")
        except NoSuchElementException:
            ReportOrgName = np.nan
        try: 
            ReportName = dr.find_element_by_xpath(xpath_repName).get_attribute("innerText")
        except NoSuchElementException:
            ReportName = np.nan
        try:
            ReportDate = dr.find_element_by_xpath(xpath_repDate).get_attribute("innerText")
        except NoSuchElementException:
            ReportDate = np.nan
        try:
            ReportTableStatus = dr.find_element_by_xpath(xpath_class).get_attribute("class")
        except NoSuchElementException:
            ReportTableStatus = np.nan
        try:
            ReportURL = dr.current_url
        except NoSuchElementException:
            ReportURL = np.nan
        try:
            ReportTableURL = dr.find_element_by_xpath(xpath_link).get_attribute('href')
        except NoSuchElementException:
            ReportTableURL = np.nan

        RetrievedDate = self.getRetDate()

        return ReportOrgName, ReportName, ReportDate, ReportTableStatus,\
                ReportURL, ReportTableURL, RetrievedDate

    def getOrgMetaData(self):
        #Org Level Metadata gathering URL
        browser = self.br
        try: #OrgName
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/form/div/p'
            org_name = browser.find_element_by_xpath(xpath).get_attribute("innerText")
        except NoSuchElementException:
            org_name = np.nan

        try: #Owner and Ownerlink
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/p'
            tusuv_general = browser.find_element_by_xpath(xpath).get_attribute("innerText")
            tusuv_general_link = browser.find_element_by_xpath(xpath).get_attribute("innerHTML")
        except NoSuchElementException:
            tusuv_general = np.nan
            tusuv_general_link = np.nan
        
        try: #Established
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/div/p'
            established = browser.find_element_by_xpath(xpath).get_attribute("innerText")
        except NoSuchElementException:
            established = np.nan
        
        try: # Dir Name
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/div[2]/div[1]/p'
            element = browser.find_element_by_xpath(xpath)
            director = element.get_attribute("innerText")
        except NoSuchElementException:
            director = np.nan
        
        try: # Dir phone
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/div[2]/div[2]/p'
            dir_phone = browser.find_element_by_xpath(xpath).get_attribute("innerText")
        except NoSuchElementException:
            dir_phone = np.nan
        
        try: # Acc name
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/div[3]/div[1]/p'
            accountant = browser.find_element_by_xpath(xpath).get_attribute("innerText")
        except NoSuchElementException:
            accountant = np.nan
        
        try: # Acc phone
            xpath = '/html/body/div[2]/div/div/div[1]/div[2]/div/div[3]/div[2]/p'
            acc_phone = browser.find_element_by_xpath(xpath).get_attribute("innerText")
        except NoSuchElementException:
            acc_phone = np.nan

        url = browser.current_url
        retDate = self.getRetDate()
            
        return org_name, tusuv_general, tusuv_general_link, established,\
                director, dir_phone, accountant, acc_phone, url, retDate
        
if __name__ == "__main__":
    main("main from stand alone")


## Brief intro ##

Under this directory, there are several Jupiter notebooks from three categories. Credentials and most of the data labels are omitted. However, the core skeleton of the code remains. 

### 1. Data Cleaning ###

The two notebook samples are from the actual working code. Because of the demand, dataset type, and schema, two different approaches are taken. The first one of lightly exploring and summary statistic viewing type. The other approach is heavily checking column by column, also most of the time row by row.

### 2. Data Scraper ###

Selenium-based Data Scraping code with a Jupiter notebook that calls the code is presented. The idea was to have a custom library/a wrapper for the chromium web driver that could be used flexibly.

### 3. Hobby Analysis ###

Scraping, cleaning the data, I also run some analysis on stock/crypto markets. With the help of an economics friend just scratched some analysis on NYSE tickers.

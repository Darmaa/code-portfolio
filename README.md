## Introduction ##

In this repository, I have put a collection of samples from my works. The codes are divided into 5 groups, as shown below. You make access directly by clicking on the hyperlink or to the directory name directly.

Each subdirectory has its README file for brief information. If you need help, please reach out to me.

### Repository content:  ###

1. [Data Analytics related samples](https://github.com/Darma/code-portolio/tree/main/1_Data%20Analytics%20related%20samples)
1. [Data Visualizations related samples](https://github.com/Darma/code-portolio/tree/main/2_%20Data%20Visualizations%20related%20samples)
1. [Capacity Building/Training related samples](https://github.com/Darma/code-portolio/tree/main/3_Capacity%20Building:Training%20related%20samples)
1. [SQL samples](https://github.com/Darma/code-portolio/tree/main/4_SQL%20samples)
1. [Engineering related samples](https://github.com/Darma/code-portolio/tree/main/5_Engineering%20related%20samples)

